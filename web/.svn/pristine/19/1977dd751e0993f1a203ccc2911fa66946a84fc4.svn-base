package ws.fla.infra;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import ws.fla.services.ExceptionReportingService;
import ws.fla.services.FacebookTokenExtensionService;
import ws.fla.services.http.HTTPServiceException;
import ws.fla.util.*;


public class AuthFilter implements Filter {
    
    private static final String LOGIN_PATH = "landing";
    private static final String HOME_PATH = "";
    
    private static final String RANDOM_USER_BUCKET_COOKIE = "random_user_bucket";
    
    private static final String FB_PERMISSIONS = "email";
    private static final String FB_AUTH_ENDPOINT_TEMPALTE = "https://www.facebook.com/dialog/oauth?client_id=%s&redirect_uri=%s&scope=%s";
    
    private List<String> excludedURLs = new ArrayList<String>();

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        String requestURL = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
        if(requestURL.charAt(0) == '/') {
            requestURL = requestURL.substring(1);
        }
        
        //Util.log("requestURL: %s", httpRequest.getRequestURI().toString());
        
        int randomUserBucket = ensureRandomBucketCookie(httpRequest, httpResponse);
        boolean isFBLoginResponse = isFBLoginResponse(httpRequest);
        Util.log("isFBLogingResponse: %b", isFBLoginResponse);
        if(isFBLoginResponse) {
            Analytics.log(httpRequest, "fb_oauth_response", "success", isSuccessfulFBLoginResponse(httpRequest));
            FacebookTokenExtensionService.submit(request.getParameter("code"));
        }
        
        boolean isLoggedIn = false;
        
        try {
            isLoggedIn = RequestUtil.isLoggedIn(httpRequest);
        } catch (HTTPServiceException e) {
            ExceptionReportingService.submit(e);
        } catch (JSONException e) {
            ExceptionReportingService.submit(e);
        }
        
        //Util.log("requestURL2: %s", requestURL);
        
        if(isLoggedIn && requestURL.indexOf(LOGIN_PATH) == 0) {
            Util.tempRedirectNoCache((HttpServletResponse)response, HOME_PATH);
            return;
        }
        
        if (isExcluded(requestURL)) {
            //Util.log("isExcluded: %s", requestURL);
            chain.doFilter(request, response);
            return;
        }
        
        Util.log("isLoggedIn: %b", isLoggedIn);
        Util.log("requestURL: %s", httpRequest.getRequestURL().toString());
        Util.log("isExcludedHome: %b", isExcludedHome(httpRequest));
        
        boolean isExcludedHomeRequest = isExcludedHome(httpRequest); 
        if(!isLoggedIn) {
            
            if(!isExcludedHomeRequest) {
                StringBuffer reqURL = httpRequest.getRequestURL();
                String requestQuery = httpRequest.getQueryString();
                if(requestQuery != null) {
                    reqURL = reqURL.append("?").append(requestQuery);
                }
                
                String redirectionURL = String.format("%s?r=%s", LOGIN_PATH, Util.encodeURIComponent(reqURL.toString()));
                Util.log("loffed out redirected to %s", redirectionURL);
                
                Util.tempRedirectNoCache(httpResponse, redirectionURL);
                return;    
            }
            else {
                
                Util.log("isFBLoginResponse: %b, %s", isFBLoginResponse, Util.getFullURL(httpRequest));
                /*if(randomUserBucket < 50 && !isFBLoginResponse) {
                    Analytics.log(httpRequest, "fb_oauth_request");
                    redirectForFBLogin(httpRequest, httpResponse);
                    return;
                }*/
            }
            
        }
        
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) {
        String excludeURLsParam = config.getInitParameter("excluded-list");
        if (excludeURLsParam != null) {
            parseExcluded(excludeURLsParam);
        }
    }
    
    private void parseExcluded(String sExclude) {
        StringTokenizer tokenizer = new StringTokenizer(sExclude, ",");
        while (tokenizer.hasMoreTokens()) {
            String sToken = tokenizer.nextToken().trim();       
            excludedURLs.add(sToken);
        }
    }

    private boolean isExcluded(String sURL) {
        if (sURL != null && sURL.trim().length() != 0) {
            for (String excludedURL : excludedURLs) {
                if (sURL.matches(excludedURL)) {
                    return true;
                }
            }                   
        }
        return false;
    }
    
    private static boolean isExcludedHome(HttpServletRequest request) throws MalformedURLException {
        //TODO: check the actual page
        return Util.urlHasParameter(request, "noredir", "1");
    }
    
    private static boolean isFBLoginResponse(HttpServletRequest request) throws MalformedURLException {
        return Util.urlHasParameter(request, "fbres", "1");
    }
    
    private static boolean isSuccessfulFBLoginResponse(HttpServletRequest request) throws MalformedURLException {
        return Util.urlHasParameter(request, "code", null);
    }
    
    private static int ensureRandomBucketCookie(HttpServletRequest request, HttpServletResponse response) {
        String cookie = Util.getCookie(request, RANDOM_USER_BUCKET_COOKIE);
        if(cookie == null) {
            cookie = String.format("%d", new Random().nextInt(100));
            Util.setCookie(response, RANDOM_USER_BUCKET_COOKIE, cookie, request.getServerName());
        }
        
        return Integer.parseInt(cookie);
    }
    
    private static void redirectForFBLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String redirectURL = Util.getFullURL(request);
        redirectURL = Util.ensureURLParameter(redirectURL, "fbres", "1");
        
        String fbAuthURL = String.format(FB_AUTH_ENDPOINT_TEMPALTE, Constants.FACEBOOK_APP_ID, Util.encodeURIComponent(redirectURL), FB_PERMISSIONS);
        Util.log("redirecting for FB Auth: %s", fbAuthURL);
        
        response.sendRedirect(fbAuthURL);
    }
    
    public static void main(String[] args) throws MalformedURLException {
        //System.out.println(isExcludedHome("http://grumbl.ee/index.html?noredir=1"));
    }
    
}
