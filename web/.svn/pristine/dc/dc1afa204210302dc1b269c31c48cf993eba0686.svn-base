package ws.fla.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ws.fla.infra.ConnectionPool;
import ws.fla.services.http.HTTPServiceException;
import ws.fla.util.FacebookManager;
import ws.fla.util.RequestUtil;
import ws.fla.util.Util;

public class GrumblesManager {
    private static List<Grumble> getGrumbles(String userID, List<String> friendIDs, int offset, int limit) throws SQLException {
        List<Grumble> rv = new ArrayList<Grumble>();
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT id, by_uid, of_uid, hidden_from_target, visible_only_to_target, published, description, ts FROM grumbles WHERE published AND ((of_uid IN (%s) AND (by_uid = ? OR NOT visible_only_to_target)) OR (of_uid = ? AND NOT hidden_from_target) OR by_uid = ?) ORDER by ts DESC LIMIT ? OFFSET ?";
            q = String.format(q, friendIDs.isEmpty() ? "-1" : Util.createPreparedStatementFragmentForInQuery(friendIDs.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            
            int i=0;
            if(!friendIDs.isEmpty()) {
                for(; i<friendIDs.size(); i++) {
                    ps.setString(i + 1, friendIDs.get(i));
                }    
            }
            
            ps.setString(++i, userID);
            ps.setString(++i, userID);
            ps.setString(++i, userID);
            ps.setInt(++i, limit);
            ps.setInt(++i, offset);
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Grumble grumble = getGrumbleFromResultSet(rs);
                rv.add(grumble);
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static Grumble getGrumbleFromResultSet(ResultSet rs) throws SQLException {
        String id = rs.getString("id");
        String byUID = rs.getString("by_uid");
        String ofUID = rs.getString("of_uid");
        boolean hiddenFromTarget = rs.getBoolean("hidden_from_target");
        boolean visibleOnlyToTarget = rs.getBoolean("visible_only_to_target");
        boolean published = rs.getBoolean("published");
        String description = rs.getString("description");
        
        long ts = rs.getTimestamp("ts").getTime();
        
        return new Grumble(id, byUID, ofUID, hiddenFromTarget, visibleOnlyToTarget, published, description, ts);
    }
    
    private static List<Grumble> getFeed(String userID, String accessToken, Set<String> friendIDs, int offset, int limit, boolean own, boolean friends) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        //TODO: cache friend list
        List<String> userIDs = new ArrayList<String>();
        if(own) {
            userIDs.add(userID);
        }
        if(friends) {
            userIDs.addAll(friendIDs);
        }
        
        List<Grumble> grumbles = getGrumbles(userID, userIDs, offset, limit);
        updateVoteCounts(grumbles);
        updateCommentCounts(grumbles);
        updateAliases(grumbles);
        
        return grumbles;
    }
    
    public static List<Grumble> getOwnFeed(String userID, String accessToken, Set<String> friendIDs, int offset, int limit) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        return getFeed(userID, accessToken, friendIDs, offset, limit, true, false);
    }
    
    public static List<Grumble> getFriendFeed(String userID, String accessToken, Set<String> friendIDs, int offset, int limit) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        return getFeed(userID, accessToken, friendIDs, offset, limit, false, true);
    }
    
    public static List<Grumble> getHomeFeed(String userID, String accessToken, Set<String> friendIDs, int offset, int limit) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        return getFeed(userID, accessToken, friendIDs, offset, limit, true, true);
    }
    
    private static void updateVoteCounts(List<Grumble> grumbles) throws SQLException {
        if(grumbles.isEmpty()) {
            return;    
        }
        
        Map<String, Grumble> grumbleIDToGrumble = new HashMap<String, Grumble>();
        for(Grumble grumble : grumbles) {
            grumbleIDToGrumble.put(grumble.id, grumble);
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT grumble_id, vote_type, COUNT(*) AS c FROM votes WHERE grumble_id IN (%s) GROUP BY grumble_id, vote_type";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(grumbles.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            for(int i=0; i<grumbles.size(); i++) {
                ps.setString(i + 1, grumbles.get(i).id);
            }
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String grumbleID = rs.getString("grumble_id");
                int voteType = rs.getInt("vote_type");
                int count = rs.getInt("c");
                
                Grumble grumble = grumbleIDToGrumble.get(grumbleID);
                if(voteType > 0) {
                    grumble.upVoteCount = count;
                }
                else if(voteType < 0) {
                    grumble.downVoteCount = count;
                }
            }
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static void updateAliases(List<Grumble> grumbles) throws SQLException {
        if(grumbles.isEmpty()) {
            return;    
        }
        
        Map<String, Grumble> grumbleIDToGrumble = new HashMap<String, Grumble>();
        for(Grumble grumble : grumbles) {
            grumbleIDToGrumble.put(grumble.id, grumble);
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT grumble_id, uid, alias_id FROM aliases WHERE grumble_id IN (%s)";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(grumbles.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            for(int i=0; i<grumbles.size(); i++) {
                ps.setString(i + 1, grumbles.get(i).id);
            }
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String grumbleID = rs.getString("grumble_id");
                String uid = rs.getString("uid");
                String aliasID = rs.getString("alias_id");
                
                Grumble grumble = grumbleIDToGrumble.get(grumbleID);
                grumble.aliases.put(uid, aliasID);
            }
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static void updateCommentCounts(List<Grumble> grumbles) throws SQLException {
        if(grumbles.isEmpty()) {
            return;    
        }
        
        Map<String, Grumble> grumbleIDToGrumble = new HashMap<String, Grumble>();
        for(Grumble grumble : grumbles) {
            grumbleIDToGrumble.put(grumble.id, grumble);
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT grumble_id, COUNT(*) AS c FROM comments WHERE grumble_id IN (%s) GROUP BY grumble_id";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(grumbles.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            for(int i=0; i<grumbles.size(); i++) {
                ps.setString(i + 1, grumbles.get(i).id);
            }
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String grumbleID = rs.getString("grumble_id");
                int count = rs.getInt("c");
                
                Grumble grumble = grumbleIDToGrumble.get(grumbleID);
                grumble.commentCount = count;
            }
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String addGrumble(String byID, String ofID, String description, boolean hiddenFromTarget, boolean visibleOnlyToTarget, boolean published) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "INSERT INTO grumbles (by_uid, of_uid, description, hidden_from_target, visible_only_to_target, published) VALUES (?, ?, ?, ?, ?, ?)";
            
            PreparedStatement ps = con.prepareStatement(q, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, byID);
            ps.setString(2, ofID);
            ps.setString(3, description);
            ps.setBoolean(4, hiddenFromTarget);
            ps.setBoolean(5, visibleOnlyToTarget);
            ps.setBoolean(6, published);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            
            return rs.getString(1);
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void updateGrumble(Grumble grumble) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "UPDATE grumbles SET description = ?, hidden_from_target = ?, visible_only_to_target = ?, published = ? WHERE id = ?";
            
            PreparedStatement ps = con.prepareStatement(q, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, grumble.description);
            ps.setBoolean(2, grumble.hiddenFromTarget);
            ps.setBoolean(3, grumble.visibleOnlyToTarget);
            ps.setBoolean(4, grumble.published);
            ps.setString(5, grumble.id);
            
            ps.executeUpdate();
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Map<String, String> getAliases(String grumbleID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT uid, alias_id FROM aliases WHERE grumble_id = ?";
            
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            
            Map<String, String> rv = new HashMap<String, String>();
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                String uid = rs.getString("uid");
                String aliasID = rs.getString("alias_id");
                
                rv.put(uid, aliasID);
            }
            return null;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void setAlias(String userID, String grumbleID, String aliasID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "INSERT INTO aliases (grumble_id, uid, alias_id) VALUES (?, ?, ?)";
            
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            ps.setString(2, userID);
            ps.setString(3, aliasID);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Grumble getGrumbleByID(String grumbleID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT id, by_uid, of_uid, hidden_from_target, visible_only_to_target, published, description, ts FROM grumbles WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                Grumble rv = getGrumbleFromResultSet(rs);
                
                List<Grumble> grumblesList = new ArrayList<Grumble>();
                grumblesList.add(rv);
                
                updateVoteCounts(grumblesList);
                updateCommentCounts(grumblesList);
                updateAliases(grumblesList);
                
                return rv;
            }
            return null;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String vote(String grumbleID, String voterID, boolean isUpVote) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "INSERT INTO votes (grumble_id, voter_uid, vote_type) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE vote_type = VALUES(vote_type)";
            
            PreparedStatement ps = con.prepareStatement(q, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, grumbleID);
            ps.setString(2, voterID);
            ps.setInt(3, isUpVote ? 1 : -1);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            
            return rs.getString(1);
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Set<Vote> getVotesByIDs(Set<String> voteIDs) throws SQLException {
        Set<Vote> rv = new HashSet<Vote>();
        if(voteIDs.isEmpty()) {
            return rv;
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT id, grumble_id, voter_uid, vote_type, ts FROM votes WHERE id IN (%s)";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(voteIDs.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            int i=0;
            for(String voteID : voteIDs) {
                ps.setString(i + 1, voteID);
            }
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String id = rs.getString("id");
                String grumbleID = rs.getString("grumble_id");
                String voterID = rs.getString("voter_uid");
                int voteType = rs.getInt("vote_type");
                long ts = rs.getTimestamp("ts").getTime();
                
                Vote vote = new Vote(id, voterID, grumbleID, voteType, ts);
                rv.add(vote);
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Map<String, Integer> getVotes(String userID, String[] grumbleIDs) throws SQLException {
        Map<String, Integer> rv = new HashMap<String, Integer>();
        if(grumbleIDs.length == 0) {
            return rv;
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT grumble_id, vote_type FROM votes WHERE grumble_id IN (%s) AND voter_uid = ?";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(grumbleIDs.length));
            
            PreparedStatement ps = con.prepareStatement(q);
            int i=0;
            for(; i<grumbleIDs.length; i++) {
                ps.setString(i + 1, grumbleIDs[i]);
            }
            
            ps.setString(++i, userID);
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String grumbleID = rs.getString("grumble_id");
                int voteType = rs.getInt("vote_type");
                
                rv.put(grumbleID, voteType);
            }
            
            //0 if the user has not voted
            for(String grumbleID : grumbleIDs) {
                if(!rv.containsKey(grumbleID)) {
                    rv.put(grumbleID, 0);
                }
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String addComment(String grumbleID, String commenterUID, String commentText) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "INSERT INTO comments (grumble_id, commenter_uid, comment) VALUES (?, ?, ?)";
            
            PreparedStatement ps = con.prepareStatement(q, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, grumbleID);
            ps.setString(2, commenterUID);
            ps.setString(3, commentText);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            
            return rs.getString(1);
        }
        finally {
            if(con != null) {
                con.close();
            }
        }    
    }
    
    private static Comment getCommentFromResultSet(ResultSet rs) throws SQLException {
        String id = rs.getString("comments.id");
        String grumbleID = rs.getString("comments.grumble_id");
        String commenterUID = rs.getString("commenter_uid");
        String commenterAliasID = rs.getString("alias_id");
        String text = rs.getString("comment");
        long ts = rs.getTimestamp("comments.ts").getTime();
        
        return new Comment(id, grumbleID, commenterUID, commenterAliasID, text, ts);
    }
    
    public static List<Comment> getComments(String grumbleID) throws SQLException {
        //TODO: paging
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT comments.id, comments.grumble_id, commenter_uid, comment, comments.ts, alias_id FROM comments JOIN aliases ON (comments.grumble_id = aliases.grumble_id AND comments.commenter_uid = aliases.uid) WHERE comments.grumble_id = ?";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            
            List<Comment> rv = new ArrayList<Comment>();
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Comment comment = getCommentFromResultSet(rs);
                rv.add(comment);
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Comment getCommentByID(String commentID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT comments.id, comments.grumble_id, commenter_uid, comment, comments.ts, alias_id FROM comments JOIN aliases ON (comments.grumble_id = aliases.grumble_id AND comments.commenter_uid = aliases.uid) WHERE comments.id = ?";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, commentID);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                return getCommentFromResultSet(rs);
            }
            
            return null;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void updateAccessTokenForUser(String userID, String accessToken, long expirationTime) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "INSERT INTO users (uid, access_token, access_token_expiration) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE access_token = VALUES(access_token), access_token_expiration = VALUES(access_token_expiration)";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, userID);
            ps.setString(2, accessToken);
            ps.setTimestamp(3, new Timestamp(expirationTime * 1000));
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String getUserAccessToken(String userID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT access_token FROM users WHERE uid = ? AND access_token_expiration > NOW()";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, userID);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getString("access_token");
            }
            
            return null;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Set<String> getCommentersForGrumble(String grumbleID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT DISTINCT(commenter_uid) FROM comments WHERE grumble_id = ?";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            
            Set<String> rv = new HashSet<String>();
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String commenterID = rs.getString("commenter_uid"); 
                rv.add(commenterID);
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static Set<String> getVotersForGrumble(String grumbleID) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT DISTINCT(voter_uid) FROM votes WHERE grumble_id = ?";
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, grumbleID);
            
            Set<String> rv = new HashSet<String>();
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String voterID = rs.getString("voter_uid"); 
                rv.add(voterID);
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void addOrUpdateUser(User user) throws JSONException, SQLException {
        addOrUpdateUsers(Arrays.asList(user));
    }
    
    public static void addOrUpdateUsers(List<User> users) throws JSONException, SQLException {
        final int BATCH_SIZE = 1000;
        
        Connection con = ConnectionPool.getConnection();
        boolean autoCommit = con.getAutoCommit();
        if(autoCommit) {
            con.setAutoCommit(false);
        }
        
        try {
            //TODO: update email only when the incoming email is not null
            String q = "INSERT INTO users (uid, name, first_name, last_name, email) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE name = VALUES(name), first_name = VALUES(first_name), last_name = VALUES(last_name), email = VALUES(email)";
            PreparedStatement ps = con.prepareStatement(q);
            
            for(int i=0; i<users.size(); i++) {
                User user = users.get(i);
                ps.setString(1, user.id);
                ps.setString(2, user.name);
                ps.setString(3, user.firstName);
                ps.setString(4, user.lastName);
                ps.setString(5, user.email);
                
                ps.addBatch();
                
                if(i != 0 && i%BATCH_SIZE == 0) {
                    ps.executeBatch();
                    con.commit();
                }
            }
            
            ps.executeBatch();
            con.commit();
            
        }
        finally {
            con.setAutoCommit(autoCommit);
            
            if(con != null) {
                con.close();
            }
        }
        
    }
    
    public static User getUserById(String uid) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT uid, name, first_name, last_name, email FROM users WHERE uid = ?";
            PreparedStatement ps = con.prepareStatement(q);
            
            ps.setString(1, uid);
            
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                return null;
            }
            
            String id = rs.getString("uid");
            String name = rs.getString("name");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String email = rs.getString("email");
            
            return new User(id, name, firstName, lastName, email);
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static boolean isUserOnGrumblee(String uid) throws SQLException {
        Set<String> uidSet = new HashSet<String>();
        uidSet.add(uid);
        return !getUsersOnGrumblee(uidSet).isEmpty();
    }
    
    public static Set<String> getUsersOnGrumblee(Set<String> uids) throws SQLException {
        Set<String> rv = new HashSet<String>();
        if(uids.isEmpty()) {
            return rv;    
        }
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "SELECT uid FROM users WHERE uid IN (%s) AND access_token IS NOT NULL";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(uids.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            int i=0;
            
            for(String uid : uids) {
                ps.setString(++i, uid);
            }
            
            
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                rv.add(rs.getString("uid"));
            }
            
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void publishGrumbles(Set<String> grumbleIDs) throws SQLException {
        if(grumbleIDs.isEmpty()) {
            return;
        }
        
        
        Connection con = ConnectionPool.getConnection();
        try {
            String q = "UPDATE grumbles SET published = 1 WHERE id IN (%s)";
            q = String.format(q, Util.createPreparedStatementFragmentForInQuery(grumbleIDs.size()));
            
            PreparedStatement ps = con.prepareStatement(q);
            int i=0;
            
            for(String grumbleID : grumbleIDs) {
                ps.setString(++i, grumbleID);
            }
            
            ps.executeUpdate();
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
        
        
    }
    
}
