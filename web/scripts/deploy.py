import time, os, sys

import boto.manage.cmdshell
from boto.ec2.connection import EC2Connection

INSTANCE_ID = "i-e83fa184"
is_illia = len(sys.argv) > 1  and sys.argv[1] == "illia"
SSH_KEY_PATH = ("/Users/shashank/.ssh/shashank-amazonac.pem", "/Users/illia/.ssh/sh_amzn.pem")[is_illia]
LOCAL_WAR_PATH = ("../war/ROOT.war", "/Users/illia/Documents/deploys/grumblee/ROOT.war")[is_illia]
REMOTE_WAR_STORE_PATH = "/mnt/backup/"
REMOTE_WAR_DEPLOY_PATH = "/vol/tomcat/grumble/" 

def get_credentials():
    f = open("aws.key", "r")
    try:
        access_secret = f.read()
        access, secret = access_secret.split()
        return access, secret
    finally:
        if f: f.close()

def get_instance():
    conn = EC2Connection(*get_credentials())
    #instances = conn.get_all_instances(instance_ids=[INSTANCE_ID])
    reservations = conn.get_all_instances()
    for reservation in reservations:
        for instance in reservation.instances:
            if instance.id == INSTANCE_ID:
                print "found", instance
                return instance

def get_client():
    instance = get_instance()
    return boto.manage.cmdshell.sshclient_from_instance(instance, SSH_KEY_PATH, user_name='ubuntu')


def deploy():
    ssh_client = get_client()
    war_name = "ROOT##%d.war"%(int(time.time()))
    
    remote_store_path = os.path.join(REMOTE_WAR_STORE_PATH, war_name)#unix only
    remote_deploy_path = os.path.join(REMOTE_WAR_DEPLOY_PATH, "ROOT.war")#no need for parallel deployment, simply replacing the war is good enough
    
    ssh_client.put_file(LOCAL_WAR_PATH, remote_store_path)
    
    #no need to backup, the older wars will be in the store area
    
    cmd = 'sudo cp "%s" "%s"'%(remote_store_path, remote_deploy_path)
    print cmd
    ssh_client.run(cmd)
    
   
if __name__ == "__main__":
    deploy()
