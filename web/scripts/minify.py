import os
import sys
import time
import re
import subprocess
import tempfile
import shutil
import zipfile
import fileinput
import traceback
from bs4 import BeautifulSoup

CLOSURE_COMPILER_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "lib/closure-compiler.jar")
YUI_COMPRESSOR_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "lib/yuicompressor.jar")

print CLOSURE_COMPILER_PATH
print YUI_COMPRESSOR_PATH

FILES_TO_MINIFY = ['index.html', 'mobile/index.html']

CDN_DOMAIN = "http://static.grumbl.ee"

def get_file_size(path):
    return os.stat(path).st_size

def concat_files(src_paths):
    out_file = tempfile.NamedTemporaryFile(delete=False)
    try:
        for src_path in src_paths:
            in_file = open(src_path)
            try:
                data = in_file.read()
                out_file.write(data)
            finally:
                in_file.close()
    finally:
        out_file.close()
        
    return out_file.name
    

def zipdir(dir_path, zip_path):
    zip = zipfile.ZipFile(zip_path, 'w')
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            zip.write(os.path.join(root, file))
            
    zip.close()
    

def get_html_soup(html_path):
    f = open(html_path)
    try:
        html = f.read()
        soup = BeautifulSoup(html)
        return soup
    finally:
        f.close()
        
def find_all_local_js_nodes(soup):
    local_script_nodes = []
    
    scripts = soup.findAll("script")
    for script in scripts:
        src = script.get('src', None)
        
        #// can be present for protocol independent script paths
        if src and len(src) > 0 and src.find("http") != 0 and src.find("//") != 0:
            local_script_nodes.append(script)
            
    return local_script_nodes
    
def find_local_js_path(script_node, root_dir):
    #print script_node
    src = script_node.get('src', None)
    if src[0] == '/':
        src = src[1:]
    elif src[:1] == './':
        src = src[2:]
    
    #params can be added for versioning    
    question_mark_index = src.find('?')
    if question_mark_index >= 0:
        src = src[:question_mark_index]
        
    return os.path.abspath(os.path.join(root_dir, src))
    
def minify_js_using_closure(src_paths, dest_path):
    if os.path.exists(dest_path):
        os.remove(dest_path)
    
    arguments = ["java", "-jar", CLOSURE_COMPILER_PATH, "--compilation_level", "SIMPLE_OPTIMIZATIONS", "--js_output_file", dest_path]
    for src_path in src_paths:
        arguments.extend(["--js", src_path])
        
    print arguments
    
    ret = subprocess.call(arguments)
    if ret != 0:
        raise Exception("error in running closure compiler, ret code: %d", ret)
    
def minify_html(html_path, type):
    root_dir = os.path.dirname(html_path)
    
    soup = get_html_soup(html_path)
    
    if type == 'js':
        local_nodes = find_all_local_js_nodes(soup)
    else:
        local_nodes = find_all_local_css_nodes(soup)
    
    local_node_paths = {}
    for node in local_nodes:
        if type == 'js':
            path = find_local_js_path(node, root_dir)
        else:
            path = find_local_css_path(node, root_dir)
            
        if os.path.exists(path):
            local_node_paths[node] = path
    
    file_name = os.path.basename(html_path)
    file_name = file_name.replace(".html", "")
    
    is_mobile = html_path.find("mobile") >= 0
    
    if is_mobile:
        if type == 'js':
            minified_relative_path = "js/%s.min.js"%(file_name)
        else:
            minified_relative_path = "css/%s.min.css"%(file_name)
    else:    
        if type == 'js':
            minified_relative_path = "js/app/%s.min.js"%(file_name)
        else:
            minified_relative_path = "css/app/%s.min.css"%(file_name)
    
    minified_dest_path = os.path.abspath(os.path.join(root_dir, minified_relative_path))
    #print minified_dest_path
    
    #preserving order of nodes is importatnt
    paths = [local_node_paths[node] for node in local_nodes if local_node_paths.has_key(node)]
    if type == 'js':
        minify_js_using_closure(paths, minified_dest_path)
    else:
        minify_css_using_yui(paths, minified_dest_path)
    
    if type == 'js':
        url_key = 'src'
    else:
        url_key = 'href'
        
    #update first script node to min js, remove all the other existing nodes
    for i, node in enumerate(local_node_paths):
        if i == 0:
            context = ""
            if is_mobile:
                context = "mobile/"
                
            node[url_key] = "%s/%s%s?%d"%(CDN_DOMAIN, context, minified_relative_path, int(time.time()))
        else:    
            node.extract()
    
    total_size_before = sum([get_file_size(path) for path in local_node_paths.values()])
    size_after = get_file_size(minified_dest_path)
    
    f = open(html_path, 'w')
    try:
        #f.write(soup.prettify().encode('utf-8'))
        f.write(soup.encode('utf-8'))
    finally:
        f.close()
    
    print "%d bytes in %d files -> %d bytes in 1 file"%(total_size_before, len(local_node_paths), size_after)
    
def update_css_url_match(url, css_path, dest_dir):
    #print url, dest_dir
    url = url.replace('"', "").replace("'", "")
    
    if url.find('data:') != 0 and url.find('http') != 0 and url.find('//') != 0:
        url = os.path.normpath(os.path.join(os.path.dirname(css_path), url))
        url = os.path.relpath(url, dest_dir)
    
    return "url('%s')"%(url)
    
def update_css_urls(css_path, dest_dir):
    f = open(css_path)
    try:
        css = f.read()
    finally:
        f.close()
    
    css = re.sub(r'url\(([^)]+)\)', lambda match: update_css_url_match(match.group(1), css_path, dest_dir), css)
    
    f= open(css_path, "w")
    try:
        f.write(css)
    finally:
        f.close()
    
def find_all_local_css_nodes(soup):
    local_style_nodes = []
    
    styles = soup.findAll("link")
    for style in styles:
        href = style.get('href', None)
        media = style.get('media', None)
        
        #// can be present for protocol independent script paths
        if (media is None or len(media.strip()) == 0) and href and len(href.strip()) > 0 and href.find("http") != 0 and href.find("//") != 0 and href.find('.css') > 0:
            local_style_nodes.append(style)
            
    return local_style_nodes

def find_local_css_path(style_node, root_dir):
    print style_node
    href = style_node.get('href', None)
    if href[0] == '/':
        href = href[1:]
    
    #params can be added for versioning    
    question_mark_index = href.find('?')
    if question_mark_index >= 0:
        href = href[:question_mark_index]
        
    return os.path.abspath(os.path.join(root_dir, href))


def get_flattened_css_imports(css_path):
    
    def suck_in_import(import_match):
        #TODO: this doesn't cover bare urls as mentioned in w3 spec
        url = re.search(r'url\(([^)]+)\)', import_match).group(1).replace('"', "").replace("'", "")
        url = os.path.normpath(os.path.join(os.path.dirname(css_path), url))
        print "sucking in", url, css_path
        return get_flattened_css_imports(url)
    
    f = open(css_path)
    try:
        css = f.read()
    finally:
        f.close()
        
    #can an import end without a semicolon? 
    css = re.sub(r'@import.+?;', lambda match: suck_in_import(match.group(0)), css)
    return css 

def flatten_css_file(css_path):
    flattened_css = get_flattened_css_imports(css_path)
    f = open(css_path, "w")
    try:
        f.write(flattened_css)
    finally:
        f.close()       

def minify_css_using_yui(src_paths, dest_path):
    if os.path.exists(dest_path):
        os.remove(dest_path)
        
    for src_path in src_paths:
        flatten_css_file(src_path)
        update_css_urls(src_path, os.path.dirname(dest_path))
        
        
    concated_file_path = concat_files(src_paths)
    
    arguments = ["java", "-jar", YUI_COMPRESSOR_PATH, "--type", "css", concated_file_path]
        
    print arguments
    
    f = open(dest_path, "w")
    try:
        ret = subprocess.call(arguments, stdout=f)
    finally:
        f.close()
        os.remove(concated_file_path)
        
    
    if ret != 0:
        raise Exception("error in running yui compressor, ret code: %d", ret)
    

def minify_war(war_path):
    temp_dir = tempfile.mkdtemp()
    
    war_name = os.path.basename(war_path)
    temp_war_path = os.path.join(temp_dir, war_name)
    
    shutil.copy(war_path, temp_war_path)
    
    original_cwd = os.getcwd()
    
    #zip.extractall extracts only to the current directory
    #zip.write needs to be relative the root of the zipping folder otherwise
    #it will get absolute paths
    os.chdir(temp_dir)
    
    zip = zipfile.ZipFile(temp_war_path)
    try:
        #in py < 2.7.4 this is potentially dangerous
        zip.extractall()
    finally:
        zip.close()
        
    print temp_war_path
    
    #remove the war otherwise it will get wrapped up in the new war    
    os.remove(temp_war_path)
    
    for file_name in FILES_TO_MINIFY:
        html_path = os.path.join(temp_dir, file_name);
        minify_html(html_path, 'js')
        minify_html(html_path, 'css')
        
    zipdir(".", war_path)
    
    os.chdir(original_cwd)
    
if __name__ == "__main__":
    try:
        minify_war(os.path.abspath(sys.argv[1]))
    except Exception, e:
        traceback.print_exc()
        raise e
    
    
    
    
    