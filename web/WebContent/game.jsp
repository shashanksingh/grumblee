<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="ws.fla.api.guessgame.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%

    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);

    String q = request.getParameter("q");
    if("create_new_game".equals(q)) {
        String guesseeUID = request.getParameter("guessee_uid");
        String guesserUID = request.getParameter("guesser_uid");
        String hintText = request.getParameter("hint");
        boolean anonymous = "1".equals(request.getParameter("anonymous"));
        
        String gameID = GrumblesManager.createNewGame(userID, guesseeUID, guesserUID, anonymous);
        String hintID = GrumblesManager.addNewHint(gameID, hintText);
        
        boolean guesserIsRegistered = GrumblesManager.isUserOnGrumblee(guesserUID);
        
        Game game = GrumblesManager.getGameByID(gameID);
        Hint hint = GrumblesManager.getHintByID(hintID);
        
        NotificationManager.notifyOnNewGame(game, hint);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("game", game.toJSON(userID));
        rv.put("hint", hint.toJSON(userID));
        rv.put("guesser_is_app_user", guesserIsRegistered);
        out.print(rv.toString());
        
    }
    else if("get_game".equals(q)) {
        String gameID = request.getParameter("game_id");
        Game game = GrumblesManager.getGameByID(gameID);
        
        if(!game.hinterUID.equals(userID) && !game.guesserUID.equals(userID))  {
            if(game.anonymous) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "not allowed to view this game");    
            }
            else {
                //TODO: check more permissions? Friends? Friends of Friends? How does this relate to the visibility
                //of the posts that we prompt users to make
            }
        }
        
        List<Hint> hints = GrumblesManager.getGameHints(gameID);
        List<Guess> guesses = GrumblesManager.getGameGuesses(gameID);
        
        response.setContentType("application/json");
        
        JSONArray hintsJSON = new JSONArray();
        for(Hint hint : hints) {
            hintsJSON.put(hint.toJSON(userID));
        }
        
        JSONArray guessesJSON = new JSONArray();
        for(Guess guess : guesses) {
           guessesJSON.put(guess.toJSON(userID));
        }
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("game", game.toJSON(userID));
        rv.put("hints", hintsJSON);
        rv.put("guesses", guessesJSON);
        out.print(rv.toString());
    }
    else if("add_guess".equals(q)) {
        String hintID = request.getParameter("hint_id");
        String guessUID = request.getParameter("guess_uid");
        
        Hint hint = GrumblesManager.getHintByID(hintID);
        Game game = GrumblesManager.getGameByID(hint.gameID);
        
        if(!game.hinterUID.equals(userID) && !game.guesserUID.equals(userID))  {
            if(game.anonymous) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "not allowed to view this game");    
            }
            else {
                //TODO: check more permissions? Friends? Friends of Friends? How does this relate to the visibility
                //of the posts that we prompt users to make
            }
        }
        
        String guessID = GrumblesManager.addGuess(hintID, guessUID);
        Guess guess = GrumblesManager.getGuessByID(guessID);
        
        NotificationManager.notifyOnNewGameGuess(game, hint, guess);
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("game", game.toJSON(userID));
        rv.put("hint", hint.toJSON(userID));
        rv.put("guess", guess.toJSON(guessID));
        
        out.print(rv.toString());
    }
    else if("add_hint".equals(q)) {
        String gameID = request.getParameter("game_id");
        String hintText = request.getParameter("hint");
        
        Game game = GrumblesManager.getGameByID(gameID);
        
        if(!game.hinterUID.equals(userID) && !game.guesserUID.equals(userID))  {
            if(game.anonymous) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "not allowed to view this game");    
            }
            else {
                //TODO: check more permissions? Friends? Friends of Friends? How does this relate to the visibility
                //of the posts that we prompt users to make
            }
        }
        
        String hintID = GrumblesManager.addHint(gameID, hintText);
        Hint hint = GrumblesManager.getHintByID(hintID);
        
        NotificationManager.notifyOnNewGameHint(game, hint);
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("game", game.toJSON(userID));
        rv.put("hint", hint.toJSON(userID));
        
        out.print(rv.toString());
    }
    
%>