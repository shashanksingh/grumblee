<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%
    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);
    
    if(userID == null || accessToken == null) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "not logged in");
        return;
    }
    
    String q = request.getParameter("q");
    if("invite_grumblee".equals(q)) {
        String grumbleID = request.getParameter("grumble_id");
        String recipientEmail = request.getParameter("email");
        EmailNotificationManager.sendInviteToGrumblee(grumbleID, recipientEmail);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        out.print(rv.toString());
    }
%>