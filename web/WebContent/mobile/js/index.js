var FEED_BATCH_SIZE = 20;
var MAX_FB_FRIEND_SELECT = 1;

var StateKeys = {
    SELECTED_GRUMBLE_ID: 1
};

//for passing data to "pages"
var state = {
    
};

function alertUnexpectedError(message) {
    alert(message || "Oops! Something went wrong. Please try again.");
}

function setThumbForAlias(node, aliasID) {
    var xPos = -(aliasID%10) * 30 + 'px';
    var yPos = -(Math.floor(aliasID/10) * 30) + 'px';
    node.css({
        'background-position': xPos + ' ' + yPos
    });
}

function updateVoteButtonsStatus(voteType, rootNode) {
    if(voteType > 0) {
        rootNode.find(".ui-icon-thumbs-up").addClass('active');
    }
    else {
        rootNode.find(".ui-icon-thumbs-up").removeClass('active');
    }
    
    if(voteType < 0) {
        rootNode.find(".ui-icon-thumbs-down").addClass('active');
    }
    else {
        rootNode.find(".ui-icon-thumbs-down").removeClass('active');
    }
}

function populateDummyFeed() {
    
    var template = $("#feed_item_template").html();
    var html = "";
    
    $.each(api.DUMMY_FEED_TEMPLATE_PARAMS, function(i, params){
        html += $.trim(util.printf(template, {
            grumble_id: -i,
            viewer_alias_id: -i,
            target_name: params[0],
            target_id: params[1],
            photo_url: util.printf('http://static.grumbl.ee/img/app/fake_profile_pics/{id}_2x1.jpg', {id: params[1]}),
            description: params[2],
            date: params[3],
            upvote_count: params[4],
            downvote_count: params[5],
            comment_count: params[6],
            categories: ''
        }));
    });
    
    $('#wall').html($(html));
    $("#wall .feed-item").each(function(i, e){
        $(e).addClass("dummy-feed-item");    
    });
    
    $("#home").trigger('create');
}

function addGrumbles(grumbles, prepend, cb) {
    var template = $("#feed_item_template").html();
    
    var currentUserID = api.getCurrentUserID();
    
    var html = "";
    
    api.getFriends(function(){
        $.each(grumbles, function(i, grumble){
            //remove possible duplicate while loading additional feed
            //this may happen if new grumble was added since last fetch 
            var existingGrumble = $('.feed-item[data-grumble-id="' + grumble.id + '"]');
            if(existingGrumble.length !== 0) {
                return;
            }
            
            var targetID = grumble.of_uid;
            var isOfCurrentUser = grumble.of_uid === currentUserID;
            
            var targetName = api.getUserName(targetID);
            var photoURL = util.printf("https://graph.facebook.com/{target_id}/picture?width=69&height=69", {target_id: targetID});
            
            var categories = [];
            if(isOfCurrentUser) {
                categories.push('of-me');
            }
            else {
                categories.push('of-friend');
            }
            
            if(grumble.is_by_viewer) {
                categories.push('by-me');
            }
            
            html += $.trim(util.printf(template, {
                grumble_id: grumble.id,
                viewer_alias_id: grumble.viewer_alias,
                categories: categories.join(' '),
                target_name: targetName,
                target_id: targetID,
                photo_url: photoURL,
                description: grumble.description,
                upvote_count: grumble.upvote_count,
                downvote_count: grumble.downvote_count,
                comment_count: grumble.comment_count
            }));    
        });
        
        if(grumbles.length > 0) {
            $("#wall .dummy-feed-item").remove();
        }
        
        if(prepend) {
            $('#wall').prepend($(html));
        }
        else { 
            $("#wall").append($(html));
        }
        
        $("#home").trigger('create');
        //$("#wall").listview("refresh");
        
        var grumbleIDs = $.map(grumbles, function(g, i){
            return g.id;    
        });
        
        api.getVoteStatus(grumbleIDs, function(grumbledIDToVoteType){
            for(var grumbleID in grumbledIDToVoteType) {
                var feedItem = $('.feed-item[data-grumble-id="' + grumbleID + '"]');
                var voteType = grumbledIDToVoteType[grumbleID] || 0;
                
                updateVoteButtonsStatus(voteType, feedItem);
            }        
        }, function(e){
            if(window.console) {
                console.log(e);
            }
        });
        
        if(cb) {
            cb();
        }
        
    }, this);
}

function populateAliasSelectors() {
    
    $('select[name="alias-selector"]').each(function(i, s){
        s = $(s);
        s.empty();
        
        var html = "";
        for(var aliasID in api.ALIASES) {
            var aliasName = api.ALIASES[aliasID];
            html += $.trim(util.printf('<option value="{alias_id}">{alias_name}</option>', {
                alias_id: aliasID,
                alias_name: aliasName
            }));
        }
        
        s.append($(html));
    });
}

function populateFriendSelectors() {
    api.getFriends(function(friends){
        $('select[name="friend-selector"]').each(function(i, s){
            s = $(s);
            s.empty();
            
            var html = "";
            $.each(friends, function(i, friend) {
                html += $.trim(util.printf('<option value="{friend_uid}">{friend_name}</option>', {
                    friend_uid: friend.id,
                    friend_name: friend.name
                })); 
            });
            
            s.append($(html));
            
            s.selectmenu().selectmenu('refresh');
        });
    }, this);
}

function populateInvitePopup() {
    
    api.getFriends(function(friends){
        var root = $("#invitedialog .friends-grid");
        var template = $("#friend-list-entry").html();
        
        var html = "", i = 0;
        $.each(friends, function(id, friend){
            
            html += $.trim(util.printf(template, {
                grid_class: 'ui-block-' + ('abcde'[i%5]),
                id: id,
                name: friend.name
            }));
            
            i++;
        });
        
        root.html(html).trigger('create');
        
        selectFriendsInInviterPopup(2 * MAX_FB_FRIEND_SELECT);
        
    }, this);
    
    
}

function loadMoreGrumbles(cb) {
    var currentCount = $("#wall").children().length;
    
    api.getFeed(currentCount, FEED_BATCH_SIZE, function(grumbles) {
        
        if(currentCount === 0 && grumbles.length === 0) {
            populateDummyFeed();
            if(cb) {
                cb();
            }
        }
        else {
            addGrumbles(grumbles, false, cb);    
        }
        
    }, function(error) {
        alertUnexpectedError("Oops! There was an error in getting your feed. Please try again.");
    });
}

function showEmptyFeedPopupIfNeeded() {
    var shouldShow = $("#wall").children().length === 0 || $("#wall .feed-item.dummy-feed-item").length > 0;
    if(shouldShow) {
        $.mobile.changePage("#empty-feed-dialog", {role: "dialog"});    
    }   
}

function onHomePage(cb) {
    if($("#wall").attr("data-feed-loading") === "true") {
        return;
    }
    
    if($("#wall").children().length === 0) {
        $(window).off('scroll', onScroll);
        $.mobile.loading('show');
        $("#wall").attr("data-feed-loading", "true");
        
        fbManager.afterFBInit(function(){
            loadMoreGrumbles(function(){
                
                showEmptyFeedPopupIfNeeded();
                
                $("#wall").attr("data-feed-loading", "false");
                $.mobile.loading('hide');
                $(window).scroll(onScroll);
            });
        }, this);
    }
    else {
        showEmptyFeedPopupIfNeeded();
    }
}

function refreshHome() {
    $("#wall").empty();
    onHomePage();
}

function onScroll() {
    if(!$.mobile.activePage || $.mobile.activePage.attr("id") !== "home") {
        return;
    }
    
    if($("#wall").attr("data-feed-loading") === "true") {
        return;
    }
    
     //magic number depends on the OS (and/or footer?)
    if($(window).scrollTop() + $(window).height() >= $(document).height() - 70) {
        //detach and re-attach to ensure we don't go into a loop as loading
        //new items automatically scrolls to the bottom
        $(window).off('scroll', onScroll);
        
        var reattach = function() {
            setTimeout(function(){
                $(window).on('scroll', onScroll);    
            }, 1000);
        };
        
        $("#home .more-feed-loading-spinner").show();
        loadMoreGrumbles(function(){
            $("#home .more-feed-loading-spinner").hide();
            reattach();
        });
        
    }
    
}

function populateCommentsOnGrumblePage(comments, cb) {
    
    var textArea = $("#grumble .comment-textarea");
    var template = $("#comment-list-entry").html();
    
    var onDone = function() {
        $("#grumble .comments-list").listview().listview('refresh');
        
        textArea.focus();
        if(cb) {
            cb();
        }
    };
    
    if(comments.length === 0) {
        onDone();
        return;
    }
    
    $.each(comments, function(i, comment){
        
        var html = $.trim(util.printf(template, {
            comment_id: comment.id,
            alias_name: api.ALIASES[comment.commenter_alias_id] || "anonymous",
            comment_text: comment.text
        }));
        
        var node = $(html);
        
        
        setThumbForAlias(node.find(".thumbnail"), comment.commenter_alias_id);
        var root = $("#grumble .comments-list");
        var existingCount = root.children().length;
        
        node.addClass(existingCount%2 == 0 ? 'even' : 'odd');
        if(existingCount == 0) {
            node.addClass("first");
        }
        
        root.append(node);
        node.fadeIn();
    });
    
    onDone();
}

function onPostCommentClick() {
    var grumbleID = $("#grumble").attr("data-current-grumble-id");
    var aliasID = $("#grumble select.alias-selector").val();
    var commentText = $("#grumble .comment-textarea").val();
    
    if(!commentText) {
        //button should be disabled, this should not happen
        return;
    }
    
    track('comment_post_click', {
        grumble_id: grumbleID,
        alias_id: aliasID,
        comment_text: commentText,
        comment_text_length: commentText ? commentText.length : 0
    });
    
    $.mobile.loading('show', {text: 'posting comment...'});
    
    api.addComment(grumbleID, aliasID, commentText, function(comment){
        $.mobile.loading('hide');
        
        //TODO: check that the current grumble is still the one that this
        //comment belongs to
        populateCommentsOnGrumblePage([comment]);
        
        $("#grumble .comment-textarea").val('');
        
        track('comment_post_success', {
            grumble_id: grumbleID,
            comment_id: comment.id
        });
        
    }, function(e){
        $.mobile.loading('hide');
        alertUnexpectedError('Oops! Something went wrong. Please try again.');
        
        track('comment_post_error', {
            error: e && e.toString ? e.toString() : e,
            grumble_id: grumbleID
        });
    });
}

function onPostGrumbleClick() {
    var friendID = $("#creategrumble select.friend-selector").val();
    var description = $("#creategrumble textarea.grumble-textarea").val();
    var aliasID = $("#creategrumble select.alias-selector").val();
    var hiddenFromTarget = $("#creategrumble #post-visibility-hidden-from-target").is(":checked");
    var visibleOnlyToTarget = $("#creategrumble #post-visibility-only-for-target").is(":checked");
    
    track('post_grumble_button_click', {
        friend_id: friendID,
        description: description,
        description_length: description.length,
        alias_id: aliasID,
        hidden_from_target: hiddenFromTarget
    });
    
    $.mobile.loading('show', {text: 'creating grumble...'});
    
    api.postGrumble(friendID, description, aliasID, hiddenFromTarget, visibleOnlyToTarget, function(grumble, grumbleeIsAppUser){
        
        $.mobile.loading('hide');
        addGrumbles([grumble], true);
        $.mobile.changePage("#home");
        
        track('post_grumble_success', {
            friend_id: friendID,
            grumble_id: grumble.id
        });
        
        if(!grumbleeIsAppUser) {
            api.getFriends(function(){
                var target = api.friends[friendID];
                //TODO: invite grumblee
            });   
        }
        
    }, function(e){
        $.mobile.loading('hide');
        alertUnexpectedError('Oops! Something went wrong. Please try again.');
        track('post_grumble_error', {
            error: e && e.toString ? e.toString() : e
        });
    });
}

function onGrumblePage() {
    var grumbleID = state[StateKeys.SELECTED_GRUMBLE_ID];
    api.getGrumbleByID(grumbleID, function(grumble){
        if(grumble === api.Constants.UNAVAILABLE_GRUMBLE) {
            alertUnexpectedError("Oops! We don't have the grumble you asked for.");
            return;
        }
        
        $("#grumble").attr("data-current-grumble-id", grumbleID);
        
        var targetID = grumble.of_uid;
        var targetName = api.getUserName(targetID);
        var photoURL = util.printf("https://graph.facebook.com/{target_id}/picture?width=40&height=40", {target_id: targetID});
        var grumblerAlias = api.ALIASES[grumble.by_alias] || "anonymous";
        
        var template = $("#grumble_details_template").html();
        
        
        var html = $.trim(util.printf(template, {
            grumble_id: grumbleID,
            photo_url: photoURL,
            grumblee_name: targetName,
            grumbler_alias: grumblerAlias,
            description: grumble.description,
            upvote_count: grumble.upvote_count,
            downvote_count: grumble.downvote_count,
            comment_count: grumble.comment_count
        }));
        
        $('#grumble [data-role="content"]').empty().append($(html));
        
        $.mobile.activePage.trigger('create');
        
        //add comments
        api.getComments(grumbleID, function(comments) {
            populateCommentsOnGrumblePage(comments);
        }, function(e) {
            alertUnexpectedError("Uh oh! Something went wrong in getting the comments on this grumble. Please try again.");
        });
        
        api.getVoteStatus([grumbleID], function(grumbledIDToVoteType){
            var voteType = grumbledIDToVoteType[grumbleID] || 0;
            updateVoteButtonsStatus(voteType, $("#grumble"));
            
        }, function(e){
            if(window.console) {
                console.log(e);
            }
        });
        
    }, function(e){
        alertUnexpectedError("Uh oh! Something went wrong in getting the grumble. Please try again.");
    });
}

function lazyLoadProfilePicsInInviterPopup() {
    var root = $("#invitedialog .friends-grid");
    
    var parentTop = root.scrollTop();
    var parentBottom = parentTop + root.height();
    
    root.find(".profile-pic").each(function(){
        if($(this).attr("data-original-loaded")) {
            return;
        }
        
        var picTop = $(this).offset().top;
        if(picTop > 0 & picTop < parentBottom + 300) {
            $(this).attr("data-original-loaded", "true");
            $(this).attr("src", $(this).attr("data-original"));
        }
    });
}

function pulsateInviteButtonInPopup() {
    $("#invitedialog .invite-selected-button").
      animate({opacity: 0.65}, 350, 'linear').
      animate({opacity: 1}, 350, 'linear', function(){
          setTimeout(pulsateInviteButtonInPopup, 500);
      });
}

function changeThumbCount(node, delta) {
    var countNode = node.closest(".grumble-action").find(".grumble-action-count");
    var currentCount = parseInt($.trim(countNode.text()));
    if(isNaN(currentCount)) {
        currentCount = 0;
    }
    
    currentCount += delta;
    countNode.text(currentCount);
}

function vote(node, upVote) {
    if(node.hasClass('active')) {
        return;
    }
    
    var other = null;
    node.closest('.feed-item, .grumble-actions').find('[class*="ui-icon-thumbs"]').each(function(i, e){
        if(e != node[0]) {
            other = $(e);
        }
    });
    
    var otherSelected = other.hasClass('active');
    if(otherSelected) {
        other.removeClass('active');
        changeThumbCount(other, -1);
    }
    
    node.addClass('active');
    changeThumbCount(node, +1);
    
    var grumbleID = node.closest('.feed-item, .grumble-actions').attr("data-grumble-id");
    
    api.vote(grumbleID, upVote, function(){
    }, function(e){
        alertUnexpectedError("Oops! There was an error in " + (upVote ? "up" : "down") + " voting the grumble. Please try again");
    });
}

function updateCountersVisibility(feedItem) {
    var allZero = true;
    
    var counts = feedItem.find(".meta .grumble-action-count");
    
    counts.each(function(i, e){
        if($.trim($(e).text()) != "0") {
            allZero = false;
        }    
    });
    
    if(allZero) {
        counts.hide();
    }
    else {
        counts.show();
    }    
}

function selectFriendNodeInInviterPopup(node) {
    node.attr("data-selected", "true");
    node.find(".overlay").show();
}

function unselectFriendNodeInInviterPopup(node) {
    node.removeAttr("data-selected");
    node.find(".overlay").hide();
}

function toggleFriendNodeInInviterPopup(node) {
    if(node.attr("data-selected")) {
        unselectFriendNodeInInviterPopup(node);
        return false;
    }
    else {
        selectFriendNodeInInviterPopup(node);
        return true;
    }    
}

function selectFriendsInInviterPopup(firstN) {
    $(".friend-list-entry").each(function(i){
        if(firstN === undefined || i < firstN) {
            selectFriendNodeInInviterPopup($(this));    
        }
    });
}

function unselectAllFriendsInInviterPopup() {
    $(".friend-list-entry").each(function(i){
        unselectFriendNodeInInviterPopup($(this));
    });
}

function inviteSelectedFriends(batch) {
    var selectedIDs = [];
    
    if($("#invitedialog").is(":visible")) {
        $("#invitedialog .friend-list-entry").each(function(i){
            var isSelected =  $(this).attr("data-selected");
            if(isSelected) {
                selectedIDs.push($(this).attr("data-fb-id"));
            }
        });    
    }
    
    if(selectedIDs.length == 0) {
        //TODO: user feedback
        return;
    }
    
    selectedIDs = selectedIDs.slice(0, MAX_FB_FRIEND_SELECT);
    
    FB.ui({method: 'apprequests',
        to: selectedIDs.join(","),
        title: 'Join me on Grumblee!',
        message: 'Join me on Grumblee!'
    }, function(response){
        if(window.console) {
            console.log(response);
        }
        
        if(response) {
            $.mobile.changePage("#home");
            $.each(selectedIDs, function(i, selectedID){
                var selector = self.printf('#invitedialog .friend-list-entry[data-fb-id="{fb_id}"]', {fb_id: selectedID});  
                $(selector).remove();
            });
            
            track('invite_selected_success', {selected_count: selectedIDs.length, batch: batch || 0});
            
            if(batch === undefined) {
                inviteSelectedFriends((batch || 0) + 1);
            }
        }
        else {
            alertUnexpectedError("Uh oh! There was an unexpected error. Please try again.");
            track('invite_selected_declined', {selected_count: selectedIDs.length, batch: batch || 0});
        }
    });
}

function setUpFB() {
    var redirectURL = fbManager.getURLParameter('r');
    var isLoggedOut = window.location.hash.indexOf("logout") > 0;
    
    function onLoggedIn() {
        if(redirectURL && /^https?:\/\//.test(redirectURL)) {
            $.mobile.changePage("#home", {transition: "flip"});
        }
        else {
            $.mobile.changePage("#home", {transition: "flip"});
        }
    }
        
    fbManager.init(function(loggedIn){
        //if the user is connected but cookies expired since last visit
        //server will redirect to login page, we'll get new access_token
        //and redirect back to home
        if(loggedIn && !isLoggedOut) {
            onLoggedIn();
            return;
        }
        
        $(function(){
            $(".facebook-connect").click(function(e){
                e.preventDefault();
                
                window.location.hash = window.location.hash.replace("logout", "");
                
                fbManager.login(function(loggedIn){
                    if(loggedIn) {
                        onLoggedIn();
                    }
                    else {
                        track('fb_connect_declined');
                        $("#error_box .error_message").html("<strong>Sorry!</strong> We need you to log in with Facebook to proceed");
                        $("#error_box").show();
                    }
                });
            });
        }); 
    });
}

function setUpMobile() {
    $.mobile.page.prototype.options.headerTheme = "c";
    $.mobile.page.prototype.options.contentTheme    = "c";
    $.mobile.page.prototype.options.footerTheme = "c";
    $.mobile.page.prototype.options.addBackBtn = true;
    $.mobile.defaultPageTransition = "slidefade";
}

function setupNavHandlers() {
    $(document).ready(function(){
        $(document).on('pagechange', function(e, data){
            var pageID = data.toPage.attr("id");
            if(pageID === "landing") {
                track('landing_page_visit');
            }
            else if(pageID === "home") {
                onHomePage();
                if(data.options && data.options.fromPage && data.options.fromPage.attr("id") === "landing") {
                    track('home_visit');    
                }
            }
            else if(pageID === "grumble") {
                onGrumblePage();
                track('grumble_button_click', {
                    grumble_button_type: data.options && data.options.fromPage ? data.options.fromPage.attr("id") : ""
                }); 
            }
            else if(pageID === "invitedialog") {
                track('invite_button_click', {
                    invite_button_type: data.options && data.options.fromPage ? data.options.fromPage.attr("id") : ""
                });
            }
        });    
    });
}

function setupEventHandling() {
    $(document).on("tap", ".feed-item .grumble-link, .feed-item .grumble-action-comment", function(e) {
        e.preventDefault();
        
        var grumbleID = $(this).closest(".feed-item").attr("data-grumble-id");
        state[StateKeys.SELECTED_GRUMBLE_ID] = grumbleID;
        
        $.mobile.changePage("#grumble", {transition: "slidefade"});
    });
    
    $(document).on("tap", "#grumble .grumble-action-comment", function(e) {
        e.preventDefault();
        
        $(this).closest("#grumble").find(".comment-textarea").focus();
    });
    
    $(document).on("tap", "#grumble .post-comment-button", function(e){
       e.preventDefault();
       onPostCommentClick();
    });
    
    $(document).on("tap", "#creategrumble .post-grumble-button", function(e){
        e.preventDefault();
        onPostGrumbleClick();
    });
    
    $(document).on('tap', '.grumble-action [data-role="button"]', function(e){
        $(this).animate({'zoom': 1.5}, 200, function(){
            $(this).animate({'zoom': 1.0}, 100);
        });
    });
    
    //at max one visibility option is checked at a time
    $('[id^="post-visibility-"]').change(function(){
        var self = this;
        
        if($(this).is(':checked')) {
            $('[id^="post-visibility-"]').each(function(i, e){
                if(e !== self) {
                    $(e).prop('checked', false).checkboxradio('refresh');
                }   
            });
        }
    });
    
    $("#invitedialog").live('pagebeforeshow', function(){
        $('#invitedialog .friends-grid').css('maxHeight', window.innerHeight * 0.6);
    });
    
    $("#invitedialog").live('pageshow', function(){
        lazyLoadProfilePicsInInviterPopup();
        pulsateInviteButtonInPopup();    
    });
    
    
    $("#invitedialog .friends-grid").scroll(function(){
        if(window.invitePopupScrollResponseTimer) {
            clearTimeout(invitePopupScrollResponseTimer);
        }
        
        window.invitePopupScrollResponseTimer = setTimeout(function(){
            lazyLoadProfilePicsInInviterPopup();
        }, 200);
    });
    
    $(document).on('tap', ".ui-icon-thumbs-up", function(e){
        e.preventDefault();
        vote($(this), true);
        updateCountersVisibility($(this).closest('.feed-item, .grumble-actions'));
        track('thumbs_up_click');
    });
    
    $(document).on('tap', ".ui-icon-thumbs-down", function(e){
        e.preventDefault();
        vote($(this), false);
        updateCountersVisibility($(this).closest('.feed-item, .grumble-actions'));
        track('thumbs_down_click');
    });
    
    $(document).on('tap', ".friend-list-entry", function(e){
        e.preventDefault();
        toggleFriendNodeInInviterPopup($(this));
    });
    
    $(document).on('tap', ".invite-selected-button", function(e){
        e.preventDefault();
        track('invite_selected_button_click');
        inviteSelectedFriends();
    });
    
    $(document).on('tap', '#home .reload-button', function(e){
        e.preventDefault();
        refreshHome();
        track("home_reload_button_click");
    });
}

function fixDialogOpacity() {
    $(function() {
        $('div[data-role="dialog"]').live('pagebeforeshow', function(e, ui) {
            $("#home").addClass("ui-dialog-background");
        });
        
        $('div[data-role="dialog"]').live('pagebeforehide', function(e, ui) {
            $(".ui-dialog-background").removeClass("ui-dialog-background ");
        });
    });
}

$(document).bind("mobileinit", function () {
    setUpMobile();
    setUpFB();
    setupNavHandlers();
    
    $(document).ready(function(){
        setupEventHandling();
        populateAliasSelectors();
        fixDialogOpacity();
        
        fbManager.afterFBInit(function(){
            populateFriendSelectors();
            populateInvitePopup();
        });
    });
});

var util = new window.ws.fla.Util();
var fbManager = new window.ws.fla.Facebook();
var api = new window.ws.fla.API();

