if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.API = function() {
    this.currentUser = null;
    this.friends = null;
    this.feed = [];
    this.grumbles = {};
    this.noMoreFeedAvailable = false;
    this.isLoadingFeed = false;
    this.currentUserVotes = {};
};

window.ws.fla.API.prototype = {
    Constants: {
        UNAVAILABLE_GRUMBLE: 1,
        FRIEND_LIST_LOADING: 2,
        EMPTY_FEED: 3,
        WallLoadingStatus: {
            LOADING: 0,
            FEED_END: 1,
            LOAD_ERROR: 2,
            LOAD_SUCCESS: 3
        }
    },
    
    Events: {
        FEED_LOAD: 'ee.grumbl.on_feed_load',
        FRIEND_LIST_LOAD: 'ee.grumbl.on_friend_list_load'
    },
    
    
    DUMMY_FEED_TEMPLATE_PARAMS: [
         ['Jane Doe', '1', 'Jane, you are not fat! Stop forcing people to keep affirming that for you.', '40 minutes ago', 103, 0, 5],
         ['Jenna Doe', '2', 'Kindest person I have ever known. Sometimes too kind for her own good sadly', 'an hour ago', 103, 5, 2],
         ['Jenny Doe', '3', 'Please stop holding up everyone by asking questions right at the end of a class', '7 hours ago', 123, 4, 8],
         ['Jones Doe', '4', 'Stop Instagramming your food, eat it', 'a day ago', 84, 7, 9],
         ['John Doe', '5', 'Dude, you might think somking makes you look cool, our lungs beg to disagree', 'a day ago', 1, 23, 11],
         ['Jane Roe', '6', 'Next time I get a game request from you, you are outta my facebook', 'a day ago', 923, 1, 6],
         ['Jenny Roe', '7', 'Be done with your baby pics already! We get it, you love your kids.', '3 days ago', 65, 4, 8],
         ['Jones Roe', '8', 'Latest invention you might have not heard of: mint!', '3 days ago', 79, 9, 0]
     ],
     
     ALIASES : {0: 'Batman', 1: 'Bane', 2: 'Spider-Man', 3: 'Sherlock Holmes', 
         4: 'Superman', 5: 'Iron Man', 6: 'Deadpool', 7: 'Mickey Mouse', 
         8: 'Sheldon Cooper', 9: 'Joker', 10: 'Mario', 11: 'Captain America', 
         12: 'Mandarin', 13: 'Hulk', 14: 'Green Arrow', 15: 'Hannibal Lecter', 
         16: 'Robin', 17: 'Cthulhu', 18: 'Wolverine', 19: 'Deathstroke', 
         20: 'Darth Vader', 21: 'Indiana Jones', 22: 'Talia al Ghul', 
         23: 'Thanos', 24: 'Penny', 25: 'Hawkeye', 26: 'Judge Dredd', 
         27: 'Barney Stinson', 28: 'Green Lantern', 29: 'Gwen Stacy', 
         30: 'Two-Face', 31: 'Harley Quinn', 32: 'Katniss Everdeen', 
         33: 'Godzilla', 34: 'Lord Voldemort', 35: 'Hello Kitty', 
         36: 'General Zod', 37: 'Rumpelstiltskin', 38: 'Freddy Krueger', 
         39: 'Khan Noonien Singh', 40: 'Albus Dumbledore', 41: 'Eleventh Doctor', 
         42: 'Michonne', 43: 'The Governor', 44: 'Miss Moneypenny', 
         45: 'Tenth Doctor', 46: 'Keith Lemon', 47: 'Lara Croft', 
         48: 'Captain Marvel', 49: 'Hermione Granger', 50: 'Black Canary', 
         51: 'Spock', 52: 'Mystique', 53: 'Norman Bates', 54: 'Dalek', 55: 'Goku', 
         56: 'Master Chief', 57: 'Sonic the Hedgehog', 58: 'Venom', 59: 'Bucky', 
         60: 'Jason Voorhees', 61: 'Aquaman', 62: 'Daryl Dixon', 63: 'Damian Wayne', 
         64: 'Poison Ivy', 65: 'Flash', 66: 'Electro', 67: 'Walter White', 
         68: 'Ron Swanson', 69: 'Professor Moriarty', 70: 'River Song', 
         71: 'Emma Frost', 72: 'Amy Pond', 73: 'Alien', 74: 'Bugs Bunny', 
         75: 'Magneto', 76: 'Jean Grey', 77: 'Doomsday', 78: 'Solomon Grundy', 
         79: 'Edward Scissorhands', 80: 'Princess Leia', 81: 'Robin Scherbatsky', 
         82: 'Pepper Potts', 83: 'Ant-Man', 84: 'Don Draper', 85: 'Severus Snape', 
         86: 'Jason Todd', 87: 'Mary Jane Watson', 88: 'Jack Sparrow', 89: 'Gambit', 
         90: 'Chucky', 91: 'War Machine', 92: 'Loki', 93: 'Darkseid', 
         94: 'The Most Interesting Man in the World', 95: 'Tarzan', 
         96: 'James T. Kirk', 97: 'Samurai Jack', 98: 'Green Goblin', 
         99: 'Daredevil'
     },
     
     getFriends: function(cb, scope) {
         var self = this;
         
         if(this.friends == this.Constants.FRIEND_LIST_LOADING) {
             $(window).on(this.Events.FRIEND_LIST_LOAD, function(){
                 cb.call(scope||window, self.friends);
             });
             return;
         }
         
         if(this.friends == null) {
             this.friends = this.Constants.FRIEND_LIST_LOADING;
             
             FB.api({
                 method: 'fql.query',
                 query: 'SELECT uid, first_name, name, sex, contact_email, friend_count, likes_count, mutual_friend_count, subscriber_count, wall_count, is_app_user FROM user WHERE uid = me() OR uid IN (SELECT uid2 FROM friend WHERE uid1 = me())'
             }, function(data){
                 var friends = $.map(data, function(f){
                     f.id = f.uid;
                     return f;
                 });
                 
                 var currentUserID = FB.getUserID();
                 
                 self.friends = {};
                 $.each(friends, function(i, f){
                     if(f.id === currentUserID) {
                         self.currentUser = f;
                     }
                     else {
                         self.friends[f.id] = f;    
                     }
                 });
                 
                 $(window).trigger(self.Events.FRIEND_LIST_LOAD);
                 
                 cb.call(scope||window, self.friends);
             });
         }
         else {
             cb.call(scope||window, this.friends);
         }
     },
     
     getCurrentUserID: function() {
         return FB.getUserID();
     },
     
     getUserName: function(uid) {
         var u = this.getUserByID(uid);
         return u ? u.name : "anonymous";
     },
     
     getUserByID: function(uid) {
         if(uid === FB.getUserID()) {
             return this.currentUser;
         }
         if(this.friends[uid]) {
             return this.friends[uid];
         }
         return null;
     },
     
     getFeed: function(offset, limit, cb, eb) {
         
         if(offset + limit < this.feed.length) {
            cb(this.feed.slice(offset, offset + limit));
            return;
         }
         
         if(this.feed === this.Constants.EMPTY_FEED || this.noMoreFeedAvailable) {
             cb([]);
             return;
         }
         
         var self = this;
         
         if(this.isLoadingFeed) {
             //TODO: what if two separate calls for two seprate slices are made
             $(window).on(this.Events.FEED_LOAD, function(){
                 cb.call(scope||window, self.feed.slice(offset, offset + limit));
             });
             return;
         }
         
         this.isLoadingFeed = true;
         
         $.ajax({
             url: '/feed.jsp',
             type: 'GET',
             dataType: 'json',
             data: {
                 feed_type: "home",
                 o: offset,
                 l: limit
             },
             success: function(data, textStatus, xhr) {
                 
                 self.isLoadingFeed = false;
                 
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     if(eb) {
                         eb(data.error);
                     }
                 }
                 else if(data.grumbles.length == 0) {
                     
                     self.noMoreFeedAvailable = true;
                     
                     if(offset === 0) {
                         self.feed = self.Constants.EMPTY_FEED;
                     }
                     
                     if(cb) {
                         cb(data.grumbles);    
                     }
                 }
                 else {
                     
                     //update cache
                     $.each(data.grumbles, function(i, grumble){
                         self.grumbles[grumble.id] = grumble;
                         self.feed.push(grumble);
                     });
                     
                     
                     if(cb) {
                         cb(data.grumbles);    
                     }
                 }
             },
             error: function(xhr, textStatus, error) {
                 self.isLoadingFeed = false;
                 
                 if(xhr.status == 401) {
                     fbManager.login();
                     return;
                 }
                 
                 if(eb) {
                     eb(error);
                 }
             }
         });
     },
     
     getVoteStatus: function(grumbleIDs, cb, eb) {
         var self = this;
         
         var getStatusFromCache = function() {
             var rv = {};
             $.each(grumbleIDs, function(i, grumbleID){
                 rv[grumbleID] = self.currentUserVotes[grumbleID] || 0;
             });
             return rv;
         };
         
         var unKnownGrumbleIDs = $.grep(grumbleIDs, function(grumbleID, i){
             return self.currentUserVotes[grumbleID] === undefined;
         });
         
         if(unKnownGrumbleIDs.length === 0) {
             return getStatusFromCache();
         }
         
         $.ajax({
             url: '/vote.jsp',
             type: 'GET',
             dataType: 'json',
             traditional: true,
             data: {
                 q: 'get_user_votes',
                 gid: unKnownGrumbleIDs
             },
             success: function(data, textStatus, xhr) {
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     eb(data.error);
                 }
                 else {
                     
                     for(var grumbleID in data.votes) {
                         self.currentUserVotes[grumbleID] = data.votes[grumbleID];
                     }
                     
                     cb(getStatusFromCache());
                 }
             },
             error: function(xhr, textStatus, error) {
                 eb(error);
             }
         });
     },
     
     vote: function(grumbleID, upVote, cb, eb) {
         $.ajax({
             url: '/vote.jsp',
             type: 'POST',
             dataType: 'json',
             data: {
                 q: 'add',
                 grumble_id: grumbleID,
                 is_upvote: upVote ? "1" : "0"
             },
             success: function(data, textStatus, xhr) {
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     eb(data.error);
                 }
                 else {
                     cb();
                 }
             },
             error: function(xhr, textStatus, error) {
                 eb(error);
             }
         });
     },
     
     getGrumbleByID: function(grumbleID, cb, eb) {
         
         var cached = this.grumbles[grumbleID];
         if(cached) {
             cb(cached);
             return;
         }
         
         if(cached == this.Constants.UNAVAILABLE_GRUMBLE) {
             cb(this.Constants.UNAVAILABLE_GRUMBLE);
             return;
         }
         
         if(cached === undefined) {
             var self = this;
             
             $.ajax({
                 url: '/grumble.jsp',
                 type: 'GET',
                 dataType: 'json',
                 data: {
                     q: 'get',
                     grumble_id: grumbleID
                 },
                 success: function(data, textStatus, xhr) {
                     if(window.console) {
                         console.log(data);
                     }
                     
                     if(data.error) {
                         eb(data.error);
                     }
                     else {
                         var grumble = self.Constants.UNAVAILABLE_GRUMBLE;
                         if(data.grumble) {
                             grumble = data.grumble;
                         }
                         
                         self.grumbles[grumbleID] = grumble;
                         
                         cb(grumble);
                     }
                 },
                 error: function(xhr, textStatus, error) {
                     eb(error);
                 }        
             });
         }
     },
     
     getComments: function(grumbleID, cb, eb) {
         
         $.ajax({
             url: '/comment.jsp',
             type: 'GET',
             dataType: 'json',
             data: {
                 q: 'get',
                 grumble_id: grumbleID
             },
             success: function(data, textStatus, xhr) {
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     eb(data.error);
                 }
                 else {
                     cb(data.comments);
                 }
             },
             error: function(xhr, textStatus, error) {
                 eb(error);
             }        
         });
     },
     
     addComment: function(grumbleID, aliasID, commentText, cb, eb) {
         
         $.ajax({
             url: '/comment.jsp',
             type: 'POST',
             dataType: 'json',
             data: {
                 q: 'add',
                 grumble_id: grumbleID,
                 alias_id: aliasID,
                 comment_text: commentText
             },
             success: function(data, textStatus, xhr) {
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     eb(data.error);
                 }
                 else {
                     cb(data.comment);
                 }
             },
             error: function(xhr, textStatus, error) {
                 eb(error);
             }        
         });
     },
     
     postGrumble: function(friendID, description, aliasID, hiddenFromTarget, visibleOnlyToTarget, cb, eb) {
         var self = this;
         
         $.ajax({
             url: '/grumble.jsp',
             type: 'POST',
             dataType: 'json',
             data: {
                 q: 'add',
                 target_id: friendID,
                 description: description,
                 alias_id: aliasID,
                 hidden_from_target: hiddenFromTarget ? 1 : 0,
                 visible_only_to_target: visibleOnlyToTarget ? 1 : 0
             },
             success: function(data, textStatus, xhr) {
                 if(window.console) {
                     console.log(data);
                 }
                 
                 if(data.error) {
                     eb(data.error);
                 }
                 else {
                     if(self.feed === self.Constants.EMPTY_FEED) {
                         self.feed = [];
                     }
                     
                     self.feed.unshift(data.grumble);
                     self.grumbles[data.grumble.id] = data.grumble;
                     
                     //self.addGrumbles([data.grumble], true);
                     cb(data.grumble, data.grumblee_is_app_user);
                 }
             },
             error: function(xhr, textStatus, error) {
                 eb(error);
             }
         });
     }
};