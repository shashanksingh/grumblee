if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.Facebook = function() {
    this.isFBInited = false;
    this.fbReLoginTimer = null;
};

window.ws.fla.Facebook.prototype = {
    FB_APP_ID: "149267478598942",
    Cookies: {
        USERID: "fb_userid",
        ACCESS_TOKEN: "fb_access_token" 
    },
    Events: {
        FB_INITED: 'ee.grumbl.fb_inited'   
    },
    
    //TODO: dedup these methods from index
    decodeURLParam : function(paramVal) {
        if (paramVal === null)
            return null;
        return unescape(paramVal.replace(/\+/g, " "));
    },
    
    getURLParameter : function(paramName, url) {
        if (url === undefined) {
            url = window.location.href;
        }

        paramName = paramName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + paramName + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);

        var rv = null;
        if (results != null)
            rv = results[1];

        return this.decodeURLParam(rv);
    },
    
    init: function(cb) {
        var self = this;
        
        if($("#fb-root").length == 0) {
            $("body").append('<div id="fb-root"></div>');    
        }
        
        window.fbAsyncInit = function() {
            FB.init({
                appId      : self.FB_APP_ID,
                channelUrl : '//' + document.domain + '/fbchannel.html',
                status     : true,
                cookie     : true,
                xfbml      : true,
                frictionlessRequests: true
            });
            
            
            FB.getLoginStatus(function(response) {
                self.isFBInited = true;
                $(window).trigger(self.Events.FB_INITED);
                
                if(response.status === 'connected') {
                    self._onFBConnected(response);
                    cb(true);
                }
                else {
                    cb(false);
                }
            }, true);
        };
        
        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/all.js";
           fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    },
    
    _onFBConnected: function(response) {
        var userID = response.authResponse.userID;
        var accessToken = response.authResponse.accessToken;
        var expiration = (new Date()).getTime() + response.authResponse.expiresIn * 1000; 
        
        var cookieExpiration = new Date(expiration);
        cookie(this.Cookies.USERID, userID, {expires: cookieExpiration});
        cookie(this.Cookies.ACCESS_TOKEN, accessToken, {expires: cookieExpiration});
        
        if(window.mixpanel) {
            mixpanel.identify(userID);
            FB.api('/me', function(response){
                mixpanel.register(response);
            });
        }
        
        //just talking to FB once refreshes the token without user intervention
        //user will get redirected only if they FB-disconnected our app 
        var self = this;
        this.fbReLoginTimer = setTimeout(function(){
            FB.getLoginStatus(function(response) {
                if(response.status !== 'connected') {
                    self.logout();
                }
                else {
                    if(window.track) {
                        track('fb_session_refresh');
                    }
                    self._onFBConnected(response);
                }
            });
        }, response.authResponse.expiresIn * 1000 - 60 * 1000);
    },
    
    login: function(cb) {
        var self = this;
        
        FB.getLoginStatus(function(response){
            if(response.status === "connected") {
                self._onFBConnected(response);
                if(cb) {
                    cb(true);
                }
                return;
            }
            
            FB.login(function(response){
                if(window.console) {
                    console.log(response);
                }
                
                if (response.status === 'connected') {
                    self._onFBConnected(response);
                    if(cb) {
                        cb(true);
                    }
                }
                else {
                    if(cb){
                        cb(false);    
                    }
                }
            }, {scope: 'email'});
        });
    },
    
    logout: function() {
        cookie(this.Cookies.USERID, undefined);
        cookie(this.Cookies.ACCESS_TOKEN, undefined);
        /*FB.logout(function(response){
            window.location.href = "logout.jsp";    
        });*/
        window.location.href = "logout.jsp";
    },
    
    getUserID: function() {
        return cookie(this.Cookies.USERID);
    },
    
    isFBInited: function() {
        return this.isFBInited;
    },
    
    afterFBInit: function(cb, scope) {
        if(this.isFBInited) {
            cb.call(scope||window);
        }
        else {
            $(window).on(this.Events.FB_INITED, function(){
                cb.call(scope||window);
            });
        }
    }
};