if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.Util = function() {
    
};

window.ws.fla.Util.prototype = {
    printf: function(tmpl, args) {
        for ( var key in args) {
            var pattern = "\\{" + key + "\\}";
            var re = new RegExp(pattern, "g");
            tmpl = tmpl.replace(re, args[key]);
        }
        return tmpl;
    },
    
    ellipsify : function(s, maxLen) {
        if (s.length <= maxLen)
            return s;
        return s.substring(0, maxLen - 4) + "...";
    },
    
    isValidEmail : function(email) {
        //http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return email.match(re) != null;
    },
    
    decodeURLParam : function(paramVal) {
        if (paramVal === null)
            return null;
        return unescape(paramVal.replace(/\+/g, " "));
    },
    
    getURLParameter : function(paramName, url) {
        if (url === undefined) {
            url = window.location.href;
        }

        paramName = paramName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + paramName + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);

        var rv = null;
        if (results != null)
            rv = results[1];

        return this.decodeURLParam(rv);
    },
    
    formatDate: function(d) {
        var month = d.getMonth()+1;
        var day = d.getDate();
        
        return this.printf("{day}/{month}/{year}", {
            day: ((''+day).length<2 ? '0' : '') + day,
            month: ((''+month).length<2 ? '0' : '') + month,
            year: d.getFullYear()
        });    
    },
    
    flashNode: function(node, highlightColor, duration) {
        var highlightBg = highlightColor || "#FFFF9C";
        var animateMs = duration || 3500;
        var originalBg = node.css("backgroundColor");
        node.stop().css("background-color", highlightBg).animate({backgroundColor: originalBg}, animateMs);
    }
};