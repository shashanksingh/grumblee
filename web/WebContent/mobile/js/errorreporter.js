//TODO: replace with a better solution (e.g. https://github.com/csnover/TraceKit/blob/master/tracekit.js)
if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.ErrorReporter = function() {
    this.existingErrorHandler = window.onerror;
    
    var self = this;
    window.onerror = function(msg, url, line) {
        self.onError(msg, url, line);
    };
};

window.ws.fla.ErrorReporter.prototype = {
    onError: function(msg, url, line) {
        //any error in our error handler code should not do remote reporting
        //risk of cycle otherwise
        try {
            if(this.existingErrorHandler) {
                try {
                    this.existingErrorHandler(msg, url, line); 
                 }
                 catch(e) {
                     this.reportError(e);
                 }
            }
            
            this.reportError(msg, url, line);
            
            //let the default action happen
            return false;    
        }
        catch(e) {
            try {
                if(window.console) {
                    console.log(e);
                }
            }
            catch(ee) {
            }
        }    
    },
    reportError: function(msg, url, line) {
        $(function(){
            
            var params = {
                message: msg,
                url: url,
                line: line
            };
            
            if(window.track) {
                track('global_js_error', params);
            }
            
            if(document.location.host === "grumbl.ee:8080") {
                return;
            }
            
            $.ajax({
                url: 'log.jsp',
                type: 'POST',
                dataType: 'json',
                data: {
                    event: 'global_js_error',
                    params: JSON.stringify(params)
                },
                success: function(data, textStatus, xhr) {
                    if(window.console) {
                        console.log(data);
                    }
                },
                error: function(xhr, textStatus, error) {
                    if(window.console) {
                        console.log(error);
                    }
                }
            });
        });
    }
};

window.errorReporter = new window.ws.fla.ErrorReporter();
