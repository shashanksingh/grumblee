<%@page import="ws.fla.services.EmailService"%>
<%@page import="ws.fla.util.*"%>
<%
String emailID = request.getParameter("id");
if(emailID == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing 'id'");
    return;
}

String recipientEmail = EmailService.unsubscribe(emailID);
if(recipientEmail == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "bad 'id'");
    return;    
}

%>
<html>
    <head>
    </head>
    <body>
        <p><i><%=recipientEmail%></i> has been successfully unsubscribed from all Grumblee emails</p>
        <a href="http://<%=Constants.DOMAIN%>">Go To Home Page</a>
    </body>
</html>