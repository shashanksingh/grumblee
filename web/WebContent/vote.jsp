<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%
    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);
    
    String query = request.getParameter("q");
    if("add".equals(query)) {
        String grumbleID = request.getParameter("grumble_id");
        boolean isUpVote = "1".equals(request.getParameter("is_upvote"));
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(grumble == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "bad grumble id");
            return;
        }
        
        if(!userID.equals(grumble.ofUID) && !RequestUtil.isFriend(request, grumble.ofUID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, String.format("%s is not a friend", grumble.ofUID));
            return;
        }
        
        String voteID = GrumblesManager.vote(grumbleID, userID, isUpVote);
        
        String ofName = RequestUtil.getFriendName(request, grumble.ofUID);
        //NotificationManager.notifyOnVote(userID, grumble, ofName, isUpVote);
        VoteNotificationService.submit(voteID, grumble, ofName);
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        out.print(rv.toString());
    }
    else if("get_user_votes".equals(query)) {
        String[] grumbleIDs = request.getParameterValues("gid");
        Map<String, Integer> votes = GrumblesManager.getVotes(userID, grumbleIDs);
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("votes", new JSONObject(votes));
        out.print(rv.toString());
    }
    
%>