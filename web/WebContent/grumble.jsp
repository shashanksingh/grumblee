<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
    final int MIN_DESCRIPTION_LENGTH = 5;
    final int MAX_DESCRIPTION_LENGTH = 150;

    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);
    
    if(userID == null || accessToken == null) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        return;
    }
    
    String query = request.getParameter("q");
    if("add".equals(query)) {
        String targetID = request.getParameter("target_id");
        String description = StringEscapeUtils.unescapeHtml(request.getParameter("description"));
        String aliasID = request.getParameter("alias_id");
        boolean hiddenFromTarget = "1".equals(request.getParameter("hidden_from_target"));
        boolean visibleOnlyToTarget = "1".equals(request.getParameter("visible_only_to_target"));
        boolean published = !"0".equals(request.getParameter("published"));
        
        if(!userID.equals(targetID) && !RequestUtil.isFriend(request, targetID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, String.format("%s is not a friend", targetID));
            return;
        }
        
        if(description.length() < MIN_DESCRIPTION_LENGTH) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "description too short");
            return;
        }
        
        if(description.length() > MAX_DESCRIPTION_LENGTH) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "description too long");
            return;
        }
        
        String grumbleID = GrumblesManager.addGrumble(userID, targetID, description, hiddenFromTarget, visibleOnlyToTarget, published);
        GrumblesManager.setAlias(userID, grumbleID, aliasID);
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(!published) {
            GrumbleAutoPublishingService.submit(grumble);    
        }
        
        Util.log("request.getServerPort(): %d", request.getServerPort());
        if(published && request.getServerPort() != -1) {
            NotificationManager.notifyOnGrumble(userID, grumble);    
        }
        
        boolean grumbleeIsRegistered = GrumblesManager.isUserOnGrumblee(targetID);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("grumble", grumble.toJSON(userID));
        rv.put("grumblee_is_app_user", grumbleeIsRegistered);
        out.print(rv.toString());    
    }
    else if("get".equals(query)) {
        String grumbleID = request.getParameter("grumble_id");
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(grumble != null) {
            //check permissions
            if(!userID.equals(grumble.byUID) && !userID.equals(grumble.ofUID) && !RequestUtil.isFriend(request, grumble.ofUID)) {
                grumble = null;
            }
            //TODO: may be check for published property, not super important though
        }
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("grumble", grumble == null ? JSONObject.NULL : grumble.toJSON(userID));
        out.print(rv.toString());
    }
    else if("update".equals(query)) {
        String grumbleID = request.getParameter("grumble_id");
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(grumble == null || !userID.equals(grumble.byUID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "only the author can update");
            return;
        }
        
        String description = request.getParameter("description");
        String published = request.getParameter("published");
        
        if(description != null) {
            grumble.description = description;
        }
        if(published != null) {
            grumble.published = "1".equals(published);
        }
        
        GrumblesManager.updateGrumble(grumble);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("grumble", grumble == null ? JSONObject.NULL : grumble.toJSON(userID));
        out.print(rv.toString());
    }
%>