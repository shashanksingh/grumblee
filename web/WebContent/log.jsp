<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="ws.fla.infra.AlertService"%>
<%@page import="ws.fla.util.*"%>
<%
    String event = request.getParameter("event");
    String paramsJSONString = StringEscapeUtils.unescapeHtml(request.getParameter("params"));
    
    Map<String, Object> params = new HashMap<String, Object>();
    if(paramsJSONString != null) {
        JSONObject paramsJSON = new JSONObject(paramsJSONString);
        Iterator<String> keys = paramsJSON.keys();
        while(keys.hasNext()) {
            String k = keys.next();
            Object v = paramsJSON.get(k);
            
            params.put(k, v);
        }
    }
    
    if("global_js_error".equals(event)) {
        //this needs a quick an alert as possible
        AlertService.sendErrorAlert("Global JS Error", paramsJSONString);
    }
    
    //TODO: log to somewhere
    
    response.setContentType("application/json");
    
    JSONObject rv = new JSONObject();
    rv.put("success", true);
    out.print(rv.toString());
%>