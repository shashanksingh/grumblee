<%@page import="org.json.*"%>
<%@page import="ws.fla.util.*"%>
<%
SignedRequest signedRequest = new SignedRequest(request.getParameter("signed_request"));
if(!signedRequest.verify(Constants.FACEBOOK_APP_SECRET)) {
    Util.log("facebook.jsp, signed_request verification failed");
    response.sendRedirect(String.format("http://%s/", request.getServerName()));
    return;
}

JSONObject params = signedRequest.getData();
Util.log("facebook.jsp request signed_request params: %s", params.toString(3));

String clickingThroughUserID = params.optString("user_id", null);
response.sendRedirect(String.format("http://%s/", request.getServerName()));

%>