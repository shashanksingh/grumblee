<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%

    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);

    String q = request.getParameter("q");
    if("record_grumblee_guess".equals(q)) {
        String grumbleID = request.getParameter("grumble_id");
        String guessUID = request.getParameter("guess_uid");
        
        GrumbleeGuess guess = GrumblesManager.recordGrumbleeGuess(grumbleID, userID, guessUID);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("guess", guess.toJSON(userID));
        out.print(rv.toString());
        
    }


%>