if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.Util = function() {
};

window.ws.fla.Util.prototype = {
    limitTextAreaLength: function(textAreaNode, maxLength, statusNode) {
        
        statusNode.html(maxLength + " characters remaining");
        
        textAreaNode.keyup(function(){
            var len = textAreaNode.val().length;
            var remaining = maxLength - len;
            
            if(remaining <= 0) {
                statusNode.html("please limit to " + maxLength + " characters");
                statusNode.addClass('textarea-limit-warning');
            }
            else {
                statusNode.html(maxLength + " characters remaining");
                statusNode.removeClass('textarea-limit-warning');
            }
        });
    }     
};