if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.GuessGame = function() {
    this.mutualFriends = {};
    this.openGameRefreshTimer = null;
};

window.ws.fla.GuessGame.prototype = {
    MIN_HINT_LENGTH: 10,
    GAME_REFRESH_INTERVAL: 5 * 1000,
    
    getMutualFriends: function(targetUID, cb) {
        var cached = this.mutualFriends[targetUID];
        if(cached !== undefined) {
            cb(cached);
            return;
        }
        
        //TODO: take care of preventing multiple parallel calls
        
        var self = this;
        FB.api({
            method: 'friends.getMutualFriends',
            target_uid: targetUID
        }, function(response){
            self.mutualFriends[targetUID] = response;
            cb(response);
        });
    },
    
    createGame: function(cb, eb) {
        var guessee = this.getGuessee();
        var guesser = this.getGuesser();
        var hint = this.getHint();
        var anonymous = this.isAnonymous();
        
        $.ajax({
            url: 'game.jsp',
            type: 'POST',
            dataType: 'json',
            data: {
                q: 'create_new_game',
                guessee_uid: guessee.id,
                guesser_uid: guesser.id,
                hint: hint,
                anonymous: anonymous ? 1 : 0
            },
            success: function(data, textStatus, xhr) {
                if(window.console) {
                    console.log(data);
                }
                cb(data);
            },
            error: function(xhr, textStatus, error) {
                eb(error);
            }
        });
    },
    
    getGameByID: function(gameID, cb, eb) {
        $.ajax({
            url: 'game.jsp',
            type: 'GET',
            dataType: 'json',
            data: {
                q: 'get_game',
                game_id: gameID
            },
            success: function(data, textStatus, xhr) {
                if(window.console) {
                    console.log(data);
                }
                cb(data.game, data.hints, data.guesses);
            },
            error: function(xhr, textStatus, error) {
                eb(error);
            }
        });
    },
    
    addHint: function(gameID, hintText, cb, eb) {
        $.ajax({
            url: 'game.jsp',
            type: 'POST',
            dataType: 'json',
            data: {
                q: 'add_hint',
                game_id: gameID,
                hint: hintText
            },
            success: function(data, textStatus, xhr) {
                if(window.console) {
                    console.log(data);
                }
                cb(data.game, data.hint);
            },
            error: function(xhr, textStatus, error) {
                eb(error);
            }
        });
    },
    
    addGuess: function(hintID, guessUID, cb ,eb) {
        $.ajax({
            url: 'game.jsp',
            type: 'POST',
            dataType: 'json',
            data: {
                q: 'add_guess',
                hint_id: hintID,
                guess_uid: guessUID
            },
            success: function(data, textStatus, xhr) {
                if(window.console) {
                    console.log(data);
                }
                cb(data.game, data.hint, data.guess);
            },
            error: function(xhr, textStatus, error) {
                eb(error);
            }
        });
    },
    
    isAnonymous: function() {
        return $('#guess-the-friend .anonymity-selector input[type="checkbox"]').is(":checked");    
    },
    
    getHint: function() {
        return $("#guess-the-friend .finish-step textarea.hint").val();
    },
    
    getGuesser: function() {
        var uid = $("#guess-the-friend").attr("data-guesser-fb-id");
        return homeManager.getUserByID(uid);
    },
    
    getGuessee: function() {
        var uid = $("#guess-the-friend").attr("data-guessee-fb-id");
        return homeManager.getUserByID(uid);
    },
    
    populateGuesseeFriendList: function(friends) {
        var self = this;
        
        var sortedFriendList = [];
        for(var friendID in friends) {
            sortedFriendList.push(friends[friendID]);
        }
        
        sortedFriendList.sort(function(fa, fb){
            return fb.mutual_friend_count - fa.mutual_friend_count;    
        });
        
        var template = $("#friend-list-entry").html();
        var root = $("#guess-the-friend .choose-guessee-step .thumbnails");
        root.empty();
        
        $.each(sortedFriendList, function(i, friend){
            //ignore <=1 mutual friends to save anonymity
            //TODO: enable for non-anonymous and/or show some exlanation
            //for these people missing
           if(friend.mutual_friend_count <= 1) {
               return;
           }
           
           var node =  $($.trim(homeManager.printf(template, {
               id: friend.id,
               name: friend.name
           })));
           root.append(node);
           
           node.click(function(e){
               e.preventDefault();
               
               var guesseeUID = $(this).attr("data-fb-id");
               $("#guess-the-friend").attr("data-guessee-fb-id", guesseeUID);
               
               self.populateMutualFriendList(guesseeUID);
               
               $("#guess-the-friend .carousel").carousel('next');
           });
        });
    },
    
    populateMutualFriendList: function(guesseeUID) {
        var self = this;
        
        var anonymous = self.isAnonymous();
        
        var friendFilter = $("#guess-the-friend .choose-guesser-step .friend-filter");
        if(anonymous) {
            friendFilter.prop("disabled", true).attr("placeholder", "be non-anonymous to search...");
        }
        else {
            friendFilter.prop("disabled", false).attr("placeholder", "search...");
        }
        
        this.getMutualFriends(guesseeUID, function(response){
            var mutualFriendIDs = homeManager.shuffleArray(response);
            
            var root = $("#guess-the-friend .choose-guesser-step .thumbnails");
            var template = $("#friend-list-entry").html();
            root.empty();
            
            $.each(mutualFriendIDs, function(i, id){
                var node = $($.trim(homeManager.printf(template, {
                    id: id,
                    name: anonymous ? "anonymous mutual friend #" + (i + 1) : homeManager.getUserName(id)
                })));
                
                node.attr("data-anonymous", anonymous ? "true" : "false");
                
                root.append(node);
                
                node.click(function(e){
                    e.preventDefault();
                    
                    var guesseeUID = $(this).attr("data-fb-id");
                    $("#guess-the-friend").attr("data-guesser-fb-id", guesseeUID);
                    
                    self.populateFinishStep();
                    
                    $("#guess-the-friend .carousel").carousel('next');
                });
            });
            
            lazyLoadProfilePics($("#guess-the-friend .choose-guesser-step"));
        });
    },
    
    populateFinishStep: function() {
        
        var guessee = this.getGuessee();
        var guesser = this.getGuesser();
        
        var anonymous = this.isAnonymous();
        
        var root = $("#guess-the-friend .finish-step"); 
        root.find(".guesser-name").text(anonymous ? "mutual friend" : guesser.name);
        root.find(".guessee-name").text(guessee.name);
        
        root.find(".guessee-info .profile-pic").attr("src", homeManager.printf("https://graph.facebook.com/{id}/picture?width=96&height=96", {id: guessee.id}));
        if(!anonymous) {
            root.find(".guesser-info .profile-pic").attr("src", homeManager.printf("https://graph.facebook.com/{id}/picture", {id: guesser.id}));    
        }
        
        var placeholderMessage = homeManager.printf("write a hint to help {guesser_name} guess that you are talking about {guessee_name}...", {
            guesser_name: anonymous ? "the mutual friend" : guesser.name,
            guessee_name: guessee.name
        });
        $("#guess-the-friend .finish-step textarea.hint").attr("placeholder", placeholderMessage);
        
        root.find(".hint-submit-guesser-not-registered-section .invite-button").attr("data-include-id", guesser.id);
        
        root.find(".hint-submit-success-section").hide();
        root.find(".hint-submit-guesser-not-registered-section").hide();
        root.find(".hint-submit-error-message").hide();
        
        root.find("textarea.hint").show();
        root.find(".btn-start-game").show();
        root.find(".btn-play-again").hide();
        root.find(".btn-game-exit").hide();
    },
    
    onSubmitHint: function() {
        
        var root = $("#guess-the-friend .finish-step");
        
        var errorMessageContainer = $("#guess-the-friend .alert");
        errorMessageContainer.text('').hide();
        
        var hint = this.getHint();
        if(hint.length < this.MIN_HINT_LENGTH) {
            var m = homeManager.printf("hint should be at least {len} characters long", {len: this.MIN_HINT_LENGTH});
            errorMessageContainer.text(m).show();
            return;
        }
        
        var anonymous = this.isAnonymous();
        
        var self = this;
        this.createGame(function(data){
            root.find(".guess-info").hide();
            root.find("textarea.hint").hide();
            root.find(".btn-start-game").hide();
            root.find(".btn-play-again").show();
            root.find(".btn-game-exit").show();
            
            if(!anonymous) {
                self.postHintOnGuesserWall(data.game, data.hint);
            }
            
            track("guess_game_start_success", {game_id: data.game.id, anonymous: data.game.anonymous});
            
            if(data.guesser_is_app_user) {
                $("#guess-the-friend .finish-step .hint-submit-success-section").show();    
            }
            else {
                $("#guess-the-friend .finish-step .hint-submit-guesser-not-registered-section").show();
            }
            
        }, function(error){
            self.showCreateGameError('Oops! Something went wrong. Please try again');
            track("guess_game_start_error", {hint: hint, hint_length: hint.length, anonymous: anonymous, error: e && e.toString ? e.toString() : e});
        });
    },
    
    postHintOnGuesserWall: function(game, hint) {
        var guessee = this.getGuessee();
        var guesser = this.getGuesser();
        
        var gameLink = homeManager.printf("http://{host}/?gg={game_id}&h={hint_id}", {host: document.location.host, game_id: game.id, hint_id: hint.id});
        var hintSnippet = hint.hint.substring(0, 10);
        
        track("guess_game_post_hint_on_wall_click", {game_id: game.id, guesser_id: guesser.id, guessee_id: guessee.id, game_link: gameLink});
        
        FB.ui({
           method: 'feed',
           to: guesser.id,
           link: gameLink,
           picture: 'http://static.grumbl.ee/img/app/grumblee_square_256.png',
           name: 'Guess The Friend!',
           caption: 'Guess The Friend!',
           description: homeManager.printf('Can you guess the mutual friend by this hint: "{hint_snippet}..."?', {hint_snippet: hintSnippet})
        }, function(response){
            if(window.console) {
                console.log(response);
            }
            if(response) {
                track("guess_game_post_hint_on_wall_success", {game_id: game.id, guesser_id: guesser.id, guessee_id: guessee.id, game_link: gameLink});
            }
            else {
                track("guess_game_post_hint_on_wall_declined", {game_id: game.id, guesser_id: guesser.id, guessee_id: guessee.id, game_link: gameLink});
            }
        });
    },
    
    showCreateGameError: function(message) {
        var root = $("#guess-the-friend .finish-step .hint-submit-error-message");
        root.text(message).show();
    },
    
    setPopupBusy: function(hide) {
        var icon = $("#guess-the-friend .modal-footer .loading-icon-small");
        if(hide) {
            icon.hide();
        }
        else {
            icon.show();    
        }   
    },
    
    highlightHint: function(hintID) {
        if(!hintID) {
            return;
        }
        
        var hintToHighlight = $("#guess-game-popup").find(homeManager.printf('[data-hint-id="{id}"]', {id: hintID}));
        if(hintToHighlight.length > 0) {
            hintToHighlight.closest('.modal-body').animate({
                scrollTop: hintToHighlight.offset().top
            });
            homeManager.flashNode(hintToHighlight);
            
        }
    },
    
    highlightGuess: function(guessID) {
        if(!guessID) {
            return;
        }
        
        var guessToHighlight = $("#guess-game-popup").find(homeManager.printf('[data-guess-id="{id}"]', {id: guessID}));
        if(guessToHighlight.length > 0) {
            guessToHighlight.closest('.modal-body').animate({
                scrollTop: guessToHighlight.offset().top
            });
            homeManager.flashNode(guessToHighlight);
        }
    },
    
    showPopupError: function(text) {
        var alertBox =  $("#guess-game-popup .modal-footer .alert");
        if(text) {
            alertBox.html(text).fadeIn();
        }
        else {
            alertBox.fadeOut();
        }
    },
    
    hidePopupError: function() {
        this.showPopupError();    
    },
    
    populateGamePopup: function(game, hints, guesses, append) {
        var root = $("#guess-game-popup");
        root.attr("data-game-id", game.id);
        
        if(!append) {
            root.find(".game-action-boxes").empty();    
        }
        
        var guesseePhotoURL = homeManager.getUserProfilePic(game.guessee_uid);
        root.find(".guessee-pic").attr("src", guesseePhotoURL);
        
        var guesser = game.guesser_uid ? homeManager.getUserByID(game.guesser_uid) : null;
        var guessee = game.guessee_uid ? homeManager.getUserByID(game.guessee_uid) : null;
        var hinter = game.hinter_uid ? homeManager.getUserByID(game.hinter_uid) : null;
        var gameInfoHTML = '';
        
        if(game.viewer_is_hinter) {
            gameInfoHTML = homeManager.printf('<span><span class="guesser-name">{guesser_name}</span> guessing <span class="guessee-name">{guessee_name}</span></span>', {
                guesser_name: guesser ? guesser.name : "A mutual friend",
                guessee_name: guessee ? guessee.name: "a mutual friend"
            });
                    
        }
        else if(game.viewer_is_guesser) {
            if(hinter) {
                gameInfoHTML = homeManager.printf('<span>You guessing a mutual friend of <span class="guessee-name">{hinter_name}</span></span>', {
                    hinter_name: hinter.name 
                });
            }
            else {
                gameInfoHTML = '<span>You guessing a friend</span>';
            }
        }
        else {
            
        }
        
        root.find(".game-info").html($.trim(gameInfoHTML));
        
        
        this.appendGamePopupActions(game, hints, guesses);
    },
    
    appendGamePopupActions: function(game, hints, guesses) {
        var root = $("#guess-game-popup");
        
        var hintTemplate = $("#guess-game-hint-entry").html();
        var guessTemplate = $("#guess-game-guess-entry").html();
        
        var actions = [];
        $.each(hints, function(i, hint){
            hint.action_type = 'hint';
            actions.push(hint);    
        });
        $.each(guesses, function(i, guess){
            guess.action_type = 'guess';
            actions.push(guess);    
        });
        
        actions.sort(function(a, b){
            return a.ts - b.ts;    
        });
        
        var newEntries = false;
        var correctGuessExists = false;
        $.each(actions, function(i, action){
            
            if(action.action_type === 'hint') {
                //already exists, ignore
                if(root.find(homeManager.printf('[data-hint-id="{id}"]', {id: action.id})).length > 0) {
                    return;
                }
                
                var actionHTML = homeManager.printf(hintTemplate, {
                    hint_id: action.id,
                    hinter_pic: homeManager.getUserProfilePic(game.hinter_id),
                    hint_text: action.hint
                });
            }
            else if(action.action_type === 'guess') {
                if(action.is_correct) {
                    correctGuessExists = true;
                }
                
                //already exists, ignore
                if(root.find(homeManager.printf('[data-guess-id="{id}"]', {id: action.id})).length > 0) {
                    return;
                }
                
                var guess = homeManager.getUserByID(action.guess_uid);
                var actionHTML = homeManager.printf(guessTemplate, {
                    guess_id: action.id,
                    guesser_pic: homeManager.getUserProfilePic(game.hinter_id),
                    guess_name: guess.name,
                    guess_pic: homeManager.getUserProfilePic(guess.id),
                    is_correct: action.is_correct
                });
            }
            
            root.find(".game-action-boxes").append($($.trim(actionHTML)));
            newEntries = true;
        });    
        
        if(newEntries) {
            setTimeout(function(){
                var lastAction = root.find(".game-action-boxes").children().last();
                if(lastAction) {
                    root.find(".modal-body").animate({
                        scrollTop:  lastAction.offset().top
                    }, 400, 'swing');    
                }    
            }, 500);    
        }
        
        if(correctGuessExists) {
            root.find(".new-hint").hide();
            root.find(".new-guess").hide();
            root.find(".new-game").show();
        }
        else {
            root.find(".new-game").hide();
            root.find(".new-hint").hide();
            root.find(".new-guess").hide();
            if(game.viewer_is_hinter) {
                root.find(".new-hint").show();
            }
            else if(game.viewer_is_guesser) {
                root.find(".new-guess").show();
            }
        }
        
    },
    
    showGame: function(gameIDToShow, hintIDToHighlight, guessIDToHighlight) {
        var alreadyShowing = $("#guess-game-popup").is(":visible");
        
        if(gameIDToShow === undefined) {
            gameIDToShow = homeManager.getURLParameter("gg");
            if(gameIDToShow) {
                track('auto_show_guess_game', {
                    game_id: gameIDToShow,
                    hint_id: hintIDToHighlight
                });
            }
        }
        if(hintIDToHighlight === undefined) {
            hintIDToHighlight = homeManager.getURLParameter("h");    
        }
        if(guessIDToHighlight === undefined) {
            guessIDToHighlight = homeManager.getURLParameter("gu");
        }
        
        if(!gameIDToShow) {
            return false;
        }
        
        var self = this;
        this.getGameByID(gameIDToShow, function(game, hints, guesses){
            homeManager.getFriends(function(){
                self.populateGamePopup(game, hints, guesses, alreadyShowing);
                self.highlightHint(hintIDToHighlight);
                self.highlightGuess(guessIDToHighlight);
            });
        }, function(error){
            showGlobalError("Oops! There was an error. Please try again");
            $("#guess-game-popup").modal('hide');
            homeManager.showWelcomePopup();
        });
        
        $("#guess-game-popup").attr("data-game-id", gameIDToShow).modal();
        return true;
    },
    
    refreshShowingGame: function() {
        if(window.console) {
            console.log("attempt to refresh showing game");
        }
        
        if(!$("#guess-game-popup").is(":visible")) {
            return;
        }
        var gameID = $("#guess-game-popup").attr("data-game-id");
        this.showGame(gameID, null, null);
    },
    
    init: function() {
        var self = this;
        
        $("#guess-the-friend .carousel").carousel('pause');
        
        $("#guess-the-friend .permission-step .btn-game-decline").click(function(e){
            e.preventDefault();
            $("#guess-the-friend").modal('hide');
            track('guess_game_declined');
        });
        
        $("#guess-the-friend .permission-step .btn-game-accept").click(function(e){
            e.preventDefault();
            $("#guess-the-friend .carousel").carousel('next');
            lazyLoadProfilePics($("#guess-the-friend .choose-guessee-step"));
            
            track('guess_game_accepted');
        });
        
        $("#guess-the-friend .choose-guessee-step .friend-list-container, #guess-the-friend .choose-guesser-step .friend-list-container").scroll(function(){
            if(window.guessGameGuesseeFriendListScrollResponseTimer) {
                clearTimeout(window.guessGameGuesseeFriendListScrollResponseTimer);
            }
            
            var root = $(this);
            window.guessGameGuesseeFriendListScrollResponseTimer = setTimeout(function(){
                lazyLoadProfilePics(root);
            }, 200);
        });
        
        $('#guess-the-friend .anonymity-selector input[type="checkbox"]').click(function(e){
            guessGame.populateMutualFriendList($("#guess-the-friend").attr("data-guessee-fb-id"));
            track("guess_game_anonymity_toggle", {anonymous: $(this).is(":checked")});
        });
        
        $("#guess-the-friend .finish-step .btn-play-again").click(function(e){
            e.preventDefault();
            
            var chooseGuesseeStepCarouselIndex = $('#guess-the-friend .choose-guessee-step').index();
            $("#guess-the-friend .carousel").carousel(chooseGuesseeStepCarouselIndex);
            $("#guess-the-friend .carousel").carousel('pause');
            
            track("guess_game_replay");
        });
        
        $("#guess-the-friend .finish-step .btn-game-exit").click(function(e){
            e.preventDefault();
            
            $("#guess-the-friend").modal('hide');
            if($("#wall .dummy-feed-item").length > 0) {
                $("#empty-feed-popup").modal({
                    backdrop: 'static',
                    keyboard: false
                });    
            }
            
            track("guess_game_exit");
        });
        
        $("#guess-the-friend .finish-step .btn-start-game").click(function(e){
            e.preventDefault();
            track("guess_game_start_click");
            window.guessGame.onSubmitHint();
        });
        
        
        $("#guess-the-friend .friend-filter").on('keyup input paste', function(e){
            var val = $(this).val().toLowerCase();
            
            var root = $(this).closest(".item");
            
            if(!val) {
                root.find(".friend-list-entry").show();
            }
            else {
                root.find(".friend-list-entry").each(function(i, element){
                    var name = $(this).attr("data-name");
                    if(name.toLowerCase().indexOf(val) >= 0) {
                        $(this).show();
                    }
                    else {
                        $(this).hide();
                    }
                });
            }
            lazyLoadProfilePics(root);
            
            var step = /([^\s]*?)-step/.exec($(this).closest(".item").attr("class"));
            var stepName = step && step.length > 1 ? step[1] : 'NA';
            track("guess_game_filter_used", {step: stepName});
        });
        
        $("#guess-the-friend .step-back-button").click(function(e){
            e.preventDefault();
            $("#guess-the-friend .carousel").carousel('prev');
            
            track("guess_game_back_button_used");
        });
        
        $("#guess-game-popup .new-hint-btn").click(function(e){
            self.hidePopupError();
            
            var gameID = $("#guess-game-popup").attr("data-game-id");
            var hint = $("#guess-game-popup .new-hint textarea").val();
            
            track("guess_game_new_hint_click", {hint: hint, hint_length: hint.length});
            
            if(hint.length < self.MIN_HINT_LENGTH) {
                track("guess_game_new_hint_too_short", {hint: hint, hint_length: hint.length});
                self.showPopupError(homeManager.printf("hint should be at least {len} characters long", {len: self.MIN_HINT_LENGTH}));
                return;
            }
            
            self.setPopupBusy();
            
            self.addHint(gameID, hint, function(game, hint){
                self.setPopupBusy(true);
                self.appendGamePopupActions(game, [hint], []);
                $("#guess-game-popup .new-hint textarea").val('');
                
                track("guess_game_new_hint_success", {game_id: game.id, hint: hint.hint, hint_length: hint.hint.length});
                
            }, function(error){
                self.setPopupBusy(true);
                self.showPopupError("Oops! There was an error. Please try again");
                
                track("guess_game_new_hint_error", {hint: hint, hint_length: hint.length, error: e && e.toString ? e.toString() : e});
            });        
        });
        
        $("#guess-game-popup .new-guess-btn").click(function(e){
            self.hidePopupError();
            
            var latestHintID = $("#guess-game-popup .game-hint").last().attr("data-hint-id");
            var guessUID = $("#guess-game-popup .new-guess .friend-list-typeahead").attr("data-selected-id");
            
            track("guess_game_new_guess_click", {hint_id: latestHintID, guess_uid: guessUID});
            
            if(!guessUID) {
                track("guess_game_new_guess_invalid", {hint_id: latestHintID, guess_uid: guessUID});
                self.showPopupError("please select from the dropdown list");
                return;
            }
            $("#guess-game-popup .new-guess .friend-list-typeahead").val('');
            
            self.setPopupBusy();
            
            self.addGuess(latestHintID, guessUID, function(game, hint, guess){
                
                self.setPopupBusy(true);
                self.appendGamePopupActions(game, [], [guess]);
                
                track("guess_game_new_guess_success", {game_id: game.id, hint_id: hint.id, guess_id: guess.id, is_correct: guess.is_correct});
                
            }, function(error){
                
                self.setPopupBusy(true);
                self.showPopupError("Oops! There was an error. Please try again");
                
                track("guess_game_new_guess_error", {hint_id: latestHintID, guess_uid: guessUID, error: e && e.toString ? e.toString() : e});
            });        
        });
        
        $("#guess-game-popup").on('shown', function(){
            if(self.openGameRefreshTimer) {
                clearTimeout(self.openGameRefreshTimer);
            }
            
            var refreshFunction = function(){
                self.refreshShowingGame();
                self.openGameRefreshTimer = setTimeout(refreshFunction, self.GAME_REFRESH_INTERVAL);
            };
            self.openGameRefreshTimer = setTimeout(refreshFunction, self.GAME_REFRESH_INTERVAL);
        });
        
        $("#guess-game-popup").on('hidden', function(){
            if(self.openGameRefreshTimer) {
                clearTimeout(self.openGameRefreshTimer);
                self.openGameRefreshTimer = null;
            } 
            track('guess_game_popup_hidden');
        });
        
        $(".play-guess-button, .play-guess-again-button").click(function(e){
            e.preventDefault();
            if($("#guess-game-popup").is(":visible")) {
                $("#guess-game-popup").modal('hide'); 
            }
            $("#guess-the-friend").modal('show');
            $("#guess-the-friend .carousel").carousel($("#guess-the-friend .choose-guessee-step").index()).carousel('pause');
            
            track("guess_game_play_button_clicked", {guess_game_play_button_type: $(this).attr("data-play-guess-button-type")});
            
            setTimeout(function(){
                lazyLoadProfilePics($("#guess-the-friend .choose-guessee-step"));
            }, 800);
        });
        
        homeManager.getFriends(function(friends){
            this.populateGuesseeFriendList(friends);    
        }, this);
    }
};