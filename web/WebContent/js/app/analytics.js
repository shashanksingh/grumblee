//dependency: jquery
var MIXPANEL_API_KEY = "dbb0c0ae648515022ca0fd25ac62ffd1";

var EVENT_SAMPLING_RATES = {
    'post_popup_selection_change': 0.1,
    'global_friend_list_filter_used': 0.1,
    'guess_game_filter_used': 0.1
};

function browserFromUA() {
    var BROWSER_REGEX_IE = /msie ([^;]*);/;
    var BROWSER_REGEX_FF = "firefox/(\\d\\d?\\.\\d)";
    
    if(!String.prototype.contains) {
        String.prototype.contains = function(substr) {
            return this.indexOf(substr) >= 0;
        };
    }
    
    var ag = window.navigator.userAgent;
    ag = ag.toLowerCase();
    
    if(ag.contains("msie")) {
        //trying to see through compatibility mode
        if (ag.contains("trident/4.0")) {
            return "IE 8.0";
        }
        else if (ag.contains("trident/5.0")) {
            return "IE 9.0";
        }
        else if (ag.contains("trident/6.0")) {
            return "IE 10.0";
        }
        
        var match = ag.match(BROWSER_REGEX_IE);
        if(match) {
            return "IE " + match[1];
        }
        return "IE";
    }
    if(ag.contains("opera")) {
        return "OP";
    }
    if(ag.contains("chrome")) {
        return "CR";
    }
    if(ag.contains("firefox")) {
        var match = ag.match(BROWSER_REGEX_FF);
        if(match) {
            return "FF " + match[1];
        }
        return "FF";
    }
    if((ag.contains("safari") && ag.contains("version")) || ag.contains("mobile safari")) {
        return "SF";
    }
    if(ag.contains("iphone;")) {
        return "iPhone";
    }
    if(ag.contains("ipad;")) {
        return "iPad";
    }
    if (ag.contains("libwww-perl") || ag.contains("web spider")
            || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
            || ag.contains("httpclient")) 
    {
        return "Spider";
    }
    if (ag.contains("blackberry")) {
        return "Blackberry";
    }
    if (ag.contains("browserng") || ag.contains("nokia")) {
        return "Nokia";
    }
    if (ag.contains("pantech")) {
        return "Pantech";
    }
    if (ag.contains("dolfin/")) {
        return "Dolfin";
    }

    return "UNKNOWN";
    
}

function browserAndVersionFromUA() {
    var browser = browserFromUA();
    var version = "";
    var brArr = browser.split(" ");
    if (brArr.length > 1) {
        browser = brArr[0];
        version = brArr[1];
    }
    return [browser, version];
}

function osFromUA() {
    if(!String.prototype.contains) {
        String.prototype.contains = function(substr) {
            return this.indexOf(substr) >= 0;
        };
    }
    
    var ag = window.navigator.userAgent;
    ag = ag.toLowerCase();
    
    if (ag.contains("windows 3.11"))
        return "Win16";
    if (ag.contains("windows 95"))
        return "Win95";
    if (ag.contains("windows 98") || ag.contains("win98"))
        return "Win98";
    if (ag.contains("windows 2000") || ag.contains("windows nt 5.0")
            || ag.contains("windows nt 5.01"))
        return "Windows 2000";
    if (ag.contains("windows xp") || ag.contains("windows nt 5.1"))
        return "Windows XP";
    if (ag.contains("windows server 2003") || ag.contains("windows nt 5.2"))
        return "Windows NT 5.2";
    if (ag.contains("windows vista") || ag.contains("windows nt 6.0"))
        return "Windows Vista";
    if (ag.contains("windows 7") || ag.contains("windows nt 6.1"))
        return "Windows 7";
    if (ag.contains("windows 8") || ag.contains("windows nt 6.2"))
        return "Windows 8";
    if (ag.contains("windows nt 4.0") || ag.contains("windows nt"))
        return "Win NT";
    if (ag.contains("windows me"))
        return "Windows ME";
    if (ag.contains("windows ce"))
        return "Windows CE";
    if (ag.contains("ipod;")) {
        return "mob_iPod";
    }
    if (ag.contains("iphone;")) {
        return "mob_iPhone";
    }
    if (ag.contains("ipad;")) {
        return "tab_iPad";
    }
    if (ag.contains("mac os"))
        return "Mac OS";
    if (ag.contains("symbianos") || ag.contains("series 60")
            || ag.contains("series60")) {
        return "mob_symbian";
    }
    if (ag.contains("windows phone")) {
        return "mob_windows";
    }
    if (ag.contains("j2me")) {
        return "mob_J2ME";
    }
    if (ag.contains("blackberry")) {
        return "mob_Blackberry";
    }
    if (ag.contains("android")) {
        if (ag.contains("mobile")) {
            return "mob_Android";
        }
        else {
            return "tab_Android";
        }
    }
    if (ag.contains("nokia")) {
        return "mob_Nokia";
    }
    if (ag.contains("bada/") || ag.contains("samsung")) {
        return "mob_Samsung";
    }
    if (ag.contains("sonyericsson")) {
        return "mob_SonyEricsson";
    }

    if (ag.contains("libwww-perl") || ag.contains("web spider")
            || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
            || ag.contains("httpclient")) {
        return "Spider";
    }

    if (ag.contains("open bsd"))
        return "OpenBSD";
    if (ag.contains("sun os") || ag.contains("sunos"))
        return "SunOS";
    if (ag.contains("linux"))
        return "Linux";

    return "UNKNOWN";
}

function decodeURLParam(paramVal) {
    if(paramVal === null) return null;
    return unescape(paramVal.replace(/\+/g, " "));
}

function getURLParameter(paramName, url) {
    if(url === undefined) {
        url =  window.location.href;
    }

    paramName = paramName.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+paramName+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec(url);

    var rv = null;
    if(results != null) {
        rv = results[1];
    }

    return decodeURLParam(rv);
}

function cookie(key, value, options) {
    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
                encodeURIComponent(key),
                '=',
                options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires='
                        + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : '' ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function(s) {
        return s;
    } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key)
            + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
}

function setupMixpanel() {
    (function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
        typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
        b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
        mixpanel.init(MIXPANEL_API_KEY);
        
     $(window).load(function(){
         fbManager.afterFBInit(function(){
             var fbUserID = FB.getUserID();
             if(fbUserID) {
                 mixpanel.identify(fbUserID);
                 
                 FB.api('/me', function(response){
                     var userProps = {
                         "$email": response.email,
                         "$last_login": new Date()
                     };
                     
                     for(var k in response) {
                         if(response.hasOwnProperty(k)) {
                             userProps[k] = response[k];
                         }
                     }
                     
                     mixpanel.people.set(userProps);    
                 });
             }    
         });
     });
}

function track(event, properties) {
    if(window.console) {
        console.log({
            event: event,
            properties: properties
        });
    }
    
    var samplingRate = EVENT_SAMPLING_RATES[event];
    if(samplingRate === undefined) {
        samplingRate = 1.0;
    }
    if(Math.random() > samplingRate) {
        return;
    }
    
    if(document.location.host === "grumbl.ee:8080") {
        return;
    }
    
    var brArr = browserAndVersionFromUA();
    var browser = brArr[0];
    var version = brArr[1];
    var os = osFromUA();
    var userID = window.FB ? FB.getUserID() : cookie('fb_userid');
    
    var source = getURLParameter("s");
    
    var props = {
        "browser_version": version,
        "group_br": browser,
        "group_br_v": browser+" "+version,
        "group_os": os,
        "group_os_br": os+" - "+browser,
        "group_os_br_v" : os+" - "+browser+" "+version,
        "source": source,
        "uid": userID,
        "random_user_bucket": cookie('random_user_bucket'),
        "current_domain": document.location.host,
        "current_url": document.location.href
    };
    
    if(properties) {
        for(var k in properties) {
            props[k] = properties[k];
        }    
    }
    
    mixpanel.track(event, props);
    
}

setupMixpanel();

