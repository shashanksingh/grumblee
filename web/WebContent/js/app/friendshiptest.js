if(!window.ws) {
    window.ws = {};
}
if(!window.ws.fla) {
    window.ws.fla = {};
}

window.ws.fla.FriendshipTestManager = function() {
    this.chooseTimer = null;
    this.grumbles = [];
    this.stories = [];
    
    this.friendAttributes = homeManager.shuffleArray(this.FRIEND_ATTRIBUTES);
};

window.ws.fla.FriendshipTestManager.prototype = {
    FRIEND_ATTRIBUTES: [
        {name: 'helpful', message: {
                male: 'He is always there to help you',
                female: 'She is always there to help you',
                neutral: 'She/He is always there to help'
            }
        },
        {name: 'selfish', message: {
                male: 'He is a tiny bit selfish at times',
                female: 'She is a tiny bit selfish at times',
                neutral: 'She/He is a tiny bit selfish at times'
            }
        },
        {name: 'lazy', message: {
                neutral: '"I\'m not saying you\'re lazy, but you should try out for \'American Idle\'"'
            }
        },
        {name: 'smart', message: {
                male: 'Einstein died because he could not stand the competition from him',
                female: 'Einstein died because he could not stand the competition from her',
                neutral: 'Einstein died because he could not stand the competition from her/him'
            }
        },
        {name: 'cheap', message: {
                neutral: '"Talk is cheap. but that\'s OK - so are you"'
            }
        },
        {name: 'boring', message: {
                male: 'If you see two people talking and one looks bored, he\'s the other one',
                female: 'If you see two people talking and one looks bored, she\'s the other one',
                neutral: 'If you see two people talking and one looks bored, she/he is the other one'
            }
        },
        {name: 'fun', message: {
                male: 'You can\'t have one boring moment around him',
                female: 'You can\'t have one boring moment around her',
                neutral: 'You can\'t have one boring moment around her/him'
            }
        },
        {name: 'dumb', message: {
                neutral: '"I don\'t know what makes you so stupid, but it really works!"'
            }
        }
    ],
    
    FRIEND_ATTRIBUTE_SELECTION_TIME_WINDOW: 13,
    MIN_STORY_LENGTH: 10,
    MAX_STORY_LENGTH: 150,
    
    getRandomFriendAttribute: function(skipIndex) {
        var index = ((skipIndex || 0) + 1)%this.friendAttributes.length;
        return {index:index, attribute: this.friendAttributes[index]};
    },
    
    updateGrumble: function() {
        if(this.grumbles.length !== this.stories.length) {
            return;
        }
        
        var grumble = this.grumbles.shift();
        var story = this.stories.shift();
        if(grumble !== undefined && story !== undefined && $.trim(story).length > 0) {
            $.ajax({
                url: 'grumble.jsp',
                type: 'POST',
                dataType: 'json',
                data: {
                    q: 'update',
                    grumble_id: grumble.id,
                    description: story,
                    published: 1
                },
                success: function(data, textStatus, xhr) {
                    if(window.console) {
                        console.log(data);
                    }
                    
                    track('updated_grumble_with_story_success', {grumble_id: grumble.id, story: story});
                },
                error: function(xhr, textStatus, error) {
                    track('updated_grumble_with_story_error', {
                        error: error && error.toString ? error.toString() : error
                    });
                }
            });
        }
    }    
};

function setUpFriendshipTestChoose() {
    
    var friendPropertyNode = $("#friendship-questionnaire .choose-step .friend-property");
    var currentAttribute = parseInt(friendPropertyNode.attr("data-friend-property-id"));
    if(isNaN(currentAttribute)) {
        currentAttribute = undefined;
    }
    
    var attributeInfo = friendshipTestManager.getRandomFriendAttribute(currentAttribute);
    friendPropertyNode.attr("data-friend-property-id", attributeInfo.index);
    friendPropertyNode.find(".friend-property-photo img").attr("src", homeManager.printf("http://static.grumbl.ee/img/app/adjectives/{prop}.jpg", {prop: attributeInfo.attribute.name}));
    friendPropertyNode.find(".friend-property-name").text(attributeInfo.attribute.name);
    
    $("#friendship-questionnaire .choose-step .friend-property-message").text(homeManager.printf("({message})", {message: attributeInfo.attribute.message.neutral}));
    
    
    var timerValue = $("#friendship-questionnaire .timer-value");
    timerValue.text(friendshipTestManager.FRIEND_ATTRIBUTE_SELECTION_TIME_WINDOW);
    
    var textField = $("#friendship-questionnaire .choose-step .friend-list-typeahead");
    var doneSelectingButton = $("#friendship-questionnaire .btn-test-done-selecting");
    var retryButton = $("#friendship-questionnaire .choose-step .btn-test-retry");
    
    doneSelectingButton.show().prop('disabled', true).addClass('disabled');
    
    textField.val('').prop('disabled', false).removeClass('disabled');
    //disabled typeahead needs re-initialization
    homeManager.getFriends(function(friends){
        homeManager.populateFriendTypeaheads(friends);
    }, this);
    
    retryButton.hide();
    
    friendshipTestManager.chooseTimer = setInterval(function(){
        
        var currentVal = parseInt(timerValue.text());
        if(currentVal <= 0) {
            doneSelectingButton.hide();
            retryButton.show();
            
            textField.prop('disabled', true).addClass("disabled");
            if(friendshipTestManager.chooseTimer) {
                clearInterval(friendshipTestManager.chooseTimer);
            }
            
            track('friendship_test_timeout', {tested_attribute: attributeInfo.name});
        }
        else {
            timerValue.text(currentVal - 1);
        }
        
    }, 1000);
    
    textField.focus();
    
    return attributeInfo;
}

function onFriendshipTestAnswer() {
    var textField = $("#friendship-questionnaire .choose-step .friend-list-typeahead");
    if(!textField.val()) {
        textField.focus();
        return null;
    }
    
    if(friendshipTestManager.chooseTimer) {
        clearInterval(friendshipTestManager.chooseTimer);
    }
    
    var root = $("#friendship-questionnaire .finish-step");
    
    var selectedFriendID = textField.attr("data-selected-id");
    var selectedFriend = homeManager.getUserByID(selectedFriendID);
    if(!selectedFriend) {
        track("e_missing_user", {uid: selectedFriendID, friend_list: homeManager.friends});
    }
    
    homeManager.precacheProxyProfilePic(selectedFriendID);
    
    var testedAttributeId = parseInt($("#friendship-questionnaire .choose-step .friend-property").attr("data-friend-property-id"));
    var testedAttribute = friendshipTestManager.friendAttributes[testedAttributeId];
    var testedAttributeName = testedAttribute.name;
    
    /*var profilePic = $("#friendship-questionnaire .finish-step .profile-pic");
    var src = homeManager.printf(profilePic.attr("data-original"), {id: selectedFriendID});
    profilePic.attr("src", src);
    */
    
    root.find(".tested-user-name").text(selectedFriend.name);
    root.find(".tested-property").text(testedAttributeName);
    
    /*$("#friendship-questionnaire .finish-step .story").attr("placeholder", homeManager.printf("tell us an example of how {name} was {attribute}", {
        name: selectedFriend.name,
        attribute: testedAttributeName
    }));
    $("#friendship-questionnaire .finish-step .btn-submit-story").show();
    $("#friendship-questionnaire .finish-step .story").show();*/
    
    $("#friendship-questionnaire .carousel").carousel('next');
    
    track('friendship_test_done_selecting', {selected_friend_id: selectedFriendID, selected_friend_name: selectedFriend.name, tested_attribute: testedAttributeName});
    
    var grumbleText = testedAttribute.message[selectedFriend.sex] || testedAttribute.message.neutral;  
    
    var randomAlias = homeManager.getRandomAlias();
    homeManager.postGrumble(selectedFriendID, grumbleText, randomAlias.id, false, false, false, function(grumble){
        friendshipTestManager.grumbles.push(grumble);
        friendshipTestManager.updateGrumble();
        
        root.attr("data-grumble-id", grumble.id);
        var previewRoot = root.find(".fb-share-preview-container");
        previewRoot.empty();
        homeManager.addPreview(grumble, previewRoot);
        
        track('frienship_test_post_grumble_success', {
            grumble_id: grumble.id,
            tested_attribute_name: testedAttributeName
        });
        
    }, function(e){
        track('frienship_test_post_grumble_error', {
            error: e && e.toString ? e.toString() : e,
            tested_attribute_name: testedAttributeName
        });
    });
    
    setTimeout(function(){
        pulsateShareButton();    
    }, 500);
    
}

function onSubmitStory() {
    var story = $("#friendship-questionnaire .finish-step .story").val();
    if(!story || story.length < friendshipTestManager.MIN_STORY_LENGTH) {
        $("#friendship-questionnaire .finish-step .story-submit-error-message").show().text(homeManager.printf("The story should be at least {min_length} characters", {min_length: friendshipTestManager.MIN_STORY_LENGTH}));
        return;        
    }
    
    friendshipTestManager.stories.push(story);
    friendshipTestManager.updateGrumble();
    
    $("#friendship-questionnaire .btn-submit-story").fadeOut();
    $("#friendship-questionnaire .finish-step .story").val('').fadeOut();
    $("#friendship-questionnaire .finish-step .story-submit-error-message").fadeOut();
}

function pulsateShareButton() {
    if(!$("#friendship-questionnaire").is(":visible") || !$("#friendship-questionnaire .finish-step").hasClass('active')) {
        return;
    }
    
    $("#friendship-questionnaire .grumble-fb-share-button").
      animate({opacity: 0.65}, 350, 'linear').
      animate({opacity: 1}, 350, 'linear', pulsateShareButton);
}


function friendshipTestSetUp() {
    $("#friendship-questionnaire .carousel").carousel('pause');
    
    $("#friendship-questionnaire .permission-step .btn-test-accept").click(function(e){
        e.preventDefault();
        $("#friendship-questionnaire .carousel").carousel('next');
        track('friendship_test_accepted');
    });
    
    $("#friendship-questionnaire .btn-test-decline").click(function(e){
        e.preventDefault();
        $("#friendship-questionnaire").modal('hide');
        if(homeManager.isDummyFeed()) {
            $("#empty-feed-popup").modal({
                backdrop: 'static',
                keyboard: false
            });    
        }
        
        var step = /([^\s]*?)-step/.exec($(this).closest(".item").attr("class"));
        var stepName = step && step.length > 1 ? step[1] : 'NA';
        track('friendship_test_declined', {test_step: stepName});
    });
    
    $("#friendship-questionnaire .info-step .btn-test-ready").click(function(e){
        e.preventDefault();
        var attributeInfo = setUpFriendshipTestChoose();
        $("#friendship-questionnaire .carousel").carousel('next');
        track('friendship_test_ready', {selected_attribute: attributeInfo.attribute.name});
    });
    
    $("#friendship-questionnaire .choose-step .btn-test-done-selecting").click(function(e){
        e.preventDefault();
        onFriendshipTestAnswer();
    });
    
    $("#friendship-questionnaire .finish-step .btn-submit-story").click(function(e){
        e.preventDefault();
        onSubmitStory();
    });
    
    $("#friendship-questionnaire .btn-test-retry").click(function(e){
        e.preventDefault();
        
        var attributeInfo = setUpFriendshipTestChoose();
        
        var chooseStepCarouselIndex = $('#friendship-questionnaire .choose-step').index();
        $("#friendship-questionnaire .carousel").carousel(chooseStepCarouselIndex);
        $("#friendship-questionnaire .carousel").carousel('pause');
        
        var step = /([^\s]*?)-step/.exec($(this).closest(".item").attr("class"));
        var stepName = step && step.length > 1 ? step[1] : 'NA';
        track('friendship_test_retry', {selected_attribute: attributeInfo.attribute.name, test_step: stepName});   
    });
    
    var textField = $("#friendship-questionnaire .choose-step .friend-list-typeahead");
    var doneSelectingButton = $("#friendship-questionnaire .choose-step .btn-test-done-selecting");
    textField.change(function(){
        if($(this).val().length > 0) {
            doneSelectingButton.show().prop('disabled', false).removeClass('disabled');
        }
        else {
            doneSelectingButton.show().prop('disabled', true).addClass('disabled');
        }
    });
    
    $("#friendship-questionnaire .grumble-fb-share-button").click(function(e){
        e.preventDefault();
        
        var grumbleID = $("#friendship-questionnaire .finish-step").attr("data-grumble-id");
        
        track('friendship_test_fb_share_grumble_click', {grumble_id: grumbleID});
        homeManager.shareGrumbleOnFacebook(grumbleID, function(postID){
            if(postID) {
                track('friendship_test_fb_share_grumble_success', {grumble_id: grumbleID, post_id: postID});
            }
            else {
                track('friendship_test_fb_share_grumble_declined', {grumble_id: grumbleID});
            }
        });
    });
}


$(function(){
   friendshipTestSetUp(); 
});

window.friendshipTestManager = new window.ws.fla.FriendshipTestManager(); 
