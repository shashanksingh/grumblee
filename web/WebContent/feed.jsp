<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%
    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);
    
    Util.log("feed: %s, %s", userID, accessToken);
    
    if(userID == null || accessToken == null) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        return;
    }
    
    Set<String> friendIDs = RequestUtil.getFriendIDs(request);
    
    int offset = Integer.parseInt(request.getParameter("o"));
    int limit = Integer.parseInt(request.getParameter("l"));
    
    String feedType = request.getParameter("feed_type");
    
    List<Grumble> grumbles = null;
    
    if("home".equals(feedType)) {
        grumbles = GrumblesManager.getHomeFeed(userID, accessToken, friendIDs, offset, limit);
    }
    else if("own".equals(feedType)) {
        grumbles = GrumblesManager.getOwnFeed(userID, accessToken, friendIDs, offset, limit);
    }
    else if("friend".equals(feedType)) {
        grumbles = GrumblesManager.getFriendFeed(userID, accessToken, friendIDs, offset, limit);
    }
    else {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "unknow feed_type: " + feedType);
        return;
    }
    
    Set<String> grumbleIDs = new HashSet<String>();
    for(Grumble grumble : grumbles) {
        grumbleIDs.add(grumble.id);
    }
    Map<String, List<GrumbleeGuess>> guesses = GrumblesManager.getGrumbleeGuesses(grumbleIDs);
    
    JSONArray grumblesArray = new JSONArray();
    for(Grumble grumble : grumbles) {
        grumblesArray.put(grumble.toJSON(userID));
    }
    
    JSONObject guessesObject = new JSONObject();
    for(String grumbleID : guesses.keySet()) {
        JSONArray guessArray = new JSONArray();
        guessesObject.put(grumbleID, guessArray);
        
        List<GrumbleeGuess> guessList = guesses.get(grumbleID);
        for(GrumbleeGuess guess : guessList) {
            guessArray.put(guess.toJSON(userID));
        }
    }
    
    response.setContentType("application/json");
    
    JSONObject rv = new JSONObject();
    rv.put("success", true);
    rv.put("grumbles", grumblesArray);
    rv.put("guesses", guessesObject);
    /*if(userID.equals("654175641")) {
        rv.put("grumbles", new JSONArray());    
    }*/
    
    out.print(rv.toString(3));
%>