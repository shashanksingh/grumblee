<%@page import="ws.fla.services.http.HTTPService"%>
<%@page import="ws.fla.services.http.HTTPServiceResponse"%>
<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%
    String userID = request.getParameter("id");
    
    HTTPServiceResponse res = ProfilePicCachingService.get(userID);
    
    Map<String, String> headers = res.getHeaders();
    response.setStatus(res.getStatusCode());
    
    /*for(String key : headers.keySet()) {
        Util.log("fb_img header: %s -> %s", key, headers.get(key));
        response.addHeader(key, headers.get(key));
    }*/
    response.setContentType("image/jpeg");
    response.getOutputStream().write(res.getData());
%>