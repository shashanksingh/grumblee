<%@page import="ws.fla.services.*"%>
<%@page import="ws.fla.util.*"%>
<%@page import="ws.fla.api.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
    String userID = RequestUtil.getUserID(request);
    String accessToken = RequestUtil.getAccessToken(request);
    
    String query = request.getParameter("q");
    if("add".equals(query)) {
        String grumbleID = request.getParameter("grumble_id");
        String commentText = StringEscapeUtils.unescapeHtml(request.getParameter("comment_text"));
        String aliasID = request.getParameter("alias_id");
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(grumble == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "bad grumble id");
            return;
        }
        
        if(userID.equals(grumble.ofUID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, String.format("you can't comment on grumble about yourself"));
            return;
        }
        
        if(!RequestUtil.isFriend(request, grumble.ofUID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, String.format("%s is not a friend", grumble.ofUID));
            return;
        }
        
        String existingAliasID = grumble.aliases.get(userID);
        if(existingAliasID != null && !aliasID.equals(existingAliasID)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, String.format("alias already exists %s", existingAliasID));
            return;
        }
        
        String commentID = GrumblesManager.addComment(grumbleID, userID, commentText);
        if(existingAliasID == null) {
            GrumblesManager.setAlias(userID, grumbleID, aliasID);    
        }
        
        Comment comment = GrumblesManager.getCommentByID(commentID);
        
        String ofName = RequestUtil.getFriendName(request, grumble.ofUID);
        NotificationManager.notifyOnComment(userID, grumble, ofName, comment);
        
        response.setContentType("application/json");
        
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("comment", comment.toJSON(userID));
        out.print(rv.toString());    
    }
    else if("get".equals(query)) {
        String grumbleID = request.getParameter("grumble_id");
        
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        if(grumble == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "bad grumble id");
            return;
        }
        
        List<Comment> comments = GrumblesManager.getComments(grumbleID);
        
        JSONArray commentsArray = new JSONArray();
        for(Comment comment : comments) {
            commentsArray.put(comment.toJSON(userID));
        }
        
        response.setContentType("application/json");
        JSONObject rv = new JSONObject();
        rv.put("success", true);
        rv.put("comments", commentsArray);
        
        out.print(rv.toString(3));
    }
    
    
%>