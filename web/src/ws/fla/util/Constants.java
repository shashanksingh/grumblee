package ws.fla.util;

public class Constants {
    public static final String DOMAIN = "grumbl.ee";
    
    public static final String FACEBOOK_APP_ID = "149267478598942";
    public static final String FACEBOOK_APP_SECRET = "8a54dad9b8242a27ff694bf9f5c209cc";
    
    public static final String FB_USERID_KEY = "fb_userid";
    public static final String FB_ACCESS_TOKEN_KEY = "fb_access_token";
    public static final String FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY = "fb_access_token_expiration_time";
    public static final String FB_FRIEND_LIST_KEY = "fb_friend_list";
}