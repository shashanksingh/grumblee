package ws.fla.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ws.fla.api.GrumblesManager;
import ws.fla.api.User;
import ws.fla.services.FacebookGraphService;
import ws.fla.services.FacebookTokenExtensionService;
import ws.fla.services.http.HTTPService;
import ws.fla.services.http.HTTPServiceException;
import ws.fla.services.http.HTTPServiceResponse;


public class FacebookManager {
    
    private static String appAccessToken;
    
    public static String getAppAccessToken() throws HTTPServiceException, UnsupportedEncodingException, IOException {
        if(appAccessToken == null) {
            String url = String.format("https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials", Constants.FACEBOOK_APP_ID, Constants.FACEBOOK_APP_SECRET);
            HTTPServiceResponse res = HTTPService.get(url);
            if(!res.isSuccess()) {
                throw new HTTPServiceException(res);
            }
            
            String data = res.getDataAsString();
            appAccessToken = data.split("access_token=")[1];
        }
        return appAccessToken;
    }
    
    public static JSONObject verifyAccessToken(String accessToken) throws HTTPServiceException, UnsupportedEncodingException, IOException, JSONException {
        String appAccessToken = getAppAccessToken();
        
        String url = String.format("https://graph.facebook.com/debug_token?input_token=%s&access_token=%s", Util.encodeURIComponent(accessToken), Util.encodeURIComponent(appAccessToken));
        HTTPServiceResponse res = HTTPService.get(url);
        if(!res.isSuccess()) {
            throw new HTTPServiceException(res);
        }
        
        String data = res.getDataAsString();
        JSONObject info = new JSONObject(data);
        info = info.getJSONObject("data");
        
        return info;
    }
    
    private static User getUserFromFQLRow(JSONObject row) throws JSONException {
        String id = row.getString("uid");
        String firstName = row.getString("first_name");
        String lastName = row.getString("last_name");
        String name = row.getString("name");
        
        String email = row.getString("email");
        if(email != null && (email == JSONObject.NULL || "null".equals(email))) {
            email = null;
        }
        
        return new User(id, name, firstName, lastName, email);
    }
    
    public static void updateUserInfo(String userID, String accessToken) throws SQLException, JSONException, UnsupportedEncodingException, IOException, HTTPServiceException {
        String query = String.format("SELECT uid, name, first_name, last_name, email FROM user WHERE uid = me()");
        JSONArray fqlRows = FacebookGraphService.executeQuery(accessToken, query);
        
        JSONObject row = fqlRows.getJSONObject(0);
        User user = getUserFromFQLRow(row);
        GrumblesManager.addOrUpdateUser(user);
        
        FacebookTokenExtensionService.submit(userID, accessToken);
    }
    
    public static void updateUserInfo(String authCode) {
        
    }
    
    
    public static List<User> getFriends(String userID, String accessToken) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        Util.log("FBManager getFriends :%s, %s", userID, accessToken);
        
        String query = String.format("SELECT uid, first_name, last_name, name, email FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())");
        JSONArray fqlRows = FacebookGraphService.executeQuery(accessToken, query);
        
        List<User> rv = new ArrayList<User>();
        for(int i=0; i<fqlRows.length(); i++) {
            JSONObject row = fqlRows.getJSONObject(i);
            User friend = getUserFromFQLRow(row);
            rv.add(friend);
        }
        
        GrumblesManager.addOrUpdateUsers(rv);
        
        return rv; 
    }
    
    public static void sendNotification(String recipientUID, String href, String template, String ref) throws SQLException, HTTPServiceException, IOException {
        String url = String.format("https://graph.facebook.com/%s/notifications", recipientUID);
        
        if(href.indexOf("grumbl.ee:8080") >= 0) {
            Util.log("ignoring sandbox notification: %s", href);
            return;
        }
        
        String accessToken = GrumblesManager.getUserAccessToken(recipientUID);
        if(accessToken == null) {
            Util.log("no valid access token for %s, can't send notification", recipientUID);
            return;
        }
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("access_token", getAppAccessToken());
        params.put("href", href);
        params.put("template", template);
        params.put("ref", ref);
        
        HTTPServiceResponse resp = HTTPService.post(url, params);
        if(!resp.isSuccess()) {
            Util.log("error in sending notification: %s", resp.getDataAsString());
            throw new HTTPServiceException(resp);
        }
        else {
            Util.log("successfully sent notification: %s", resp.getDataAsString());
        }
    }
    
    public static String getAccessTokenForCode(String code) throws UnsupportedEncodingException, JSONException, IOException {
        String url = "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s";
        url = String.format(url, Util.encodeURIComponent(Constants.FACEBOOK_APP_ID), 
                Util.encodeURIComponent("http://grumbl.ee/index.html?noredir=1&fbres=1"),
                Util.encodeURIComponent(Constants.FACEBOOK_APP_SECRET), 
                Util.encodeURIComponent(code));
        
        System.out.println(url);
        
        String data = HTTPService.get(url).getDataAsString();
        System.out.println(data);
        
        try {
            JSONObject response = new JSONObject(data);
            if(response.optString("error") != null) {
                Util.log("error in getAccessTokenForCode: %s", data);
                return null;
            }
            
            return response.getString("access_token");
        }
        catch(JSONException jsonEx) {
            Pattern p = Pattern.compile("access_token=(.+?)($|&)");
            Matcher m = p.matcher(data);
            if(!m.find()) {
                return null;
            }
            return m.group(1);
        }
        
    }
    
    public static void main(String[] args) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException {
        HTTPService.init();
        //Util.log("%s", verifyAccessToken("AAACEdEose0cBALm4eBP0xrsK3tBdh7Ds7fabPbC2zkQybKnaUzd0fVT9qhPtCSjJktcwSTnO6KuoXxbp4ibhTMj5ZAa2bM9OvEfveuQZDZD"));
        
        //System.out.println(String.format("res: %s", getRequests(Arrays.asList(new String[]{"552630691446697"})).toString(3)));
        //String accessToken = getAccessTokenForCode("AQCF5K6FS7BWYVo6a6Qer9yZFHGklmDlNcsewsbzQCGfvtyR8yavbRmUzjcxKzRx_tFED9TicD4q0JJcm9e8oX2kJ8TxkyQJ0UuyfrIM0NnVTGTymaeSWLjSblxkb7lrYxIVW0ggePHNOBc6xh8PIxIQYNsEbpeVhlznbVCvnhyQuJ217JaI2C3k4oIbhLtzKDiir14YcsecNIyBEfiCmz2Iu7JI6xnDMYPwcH8McPr1-gwY7Zcf7_pHOi-Q7doIf51ui3a-kCmbo94Z_XdEvxOUwEd-cCtNfQAXZqCyILfo-8KcqLg8aNVq19pOut_DZmk");
        //System.out.println(verifyAccessToken(accessToken).toString(3));
        System.out.println(getAppAccessToken());
        HTTPService.destroy();
    }
}
