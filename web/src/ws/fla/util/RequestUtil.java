package ws.fla.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import ws.fla.api.User;
import ws.fla.services.http.HTTPServiceException;

public class RequestUtil {
    
    public static boolean isLoggedIn(HttpServletRequest request) throws UnsupportedEncodingException, HTTPServiceException, IOException, JSONException {
        HttpSession session = request.getSession();
        
        if(hasValidAccessToken(session)) {
            return true;
        }
        
        String accessToken = Util.getCookie(request, Constants.FB_ACCESS_TOKEN_KEY);
        if(accessToken == null) {
            return false;
        }
        
        JSONObject tokenInfo = FacebookManager.verifyAccessToken(accessToken);
        if(!tokenInfo.getBoolean("is_valid")) {
            return false;
        }
        
        String userID = tokenInfo.getString("user_id");
        long expirationTime = tokenInfo.getLong("expires_at");
        
        session.setAttribute(Constants.FB_USERID_KEY, userID);
        session.setAttribute(Constants.FB_ACCESS_TOKEN_KEY, accessToken);
        session.setAttribute(Constants.FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY, expirationTime);
        
        return true;
    }
    
    public static String getUserID(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if(!hasValidAccessToken(session)) {
            return null;
        }
        return (String)session.getAttribute(Constants.FB_USERID_KEY);
    }
    
    public static String getAccessToken(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if(!hasValidAccessToken(request.getSession())) {
            return null;
        }
        return (String)session.getAttribute(Constants.FB_ACCESS_TOKEN_KEY);
    }
    
    public static List<User> getFriends(HttpServletRequest request) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        HttpSession session = request.getSession();
        if(!hasValidAccessToken(session)) {
            return null;
        }
        
        @SuppressWarnings("unchecked")
        List<User> rv = (List<User>)session.getAttribute(Constants.FB_FRIEND_LIST_KEY);
        if(rv == null) {
            String userID = getUserID(request);
            String accessToken = getAccessToken(request);
            
            FacebookManager.updateUserInfo(userID, accessToken);
            rv = FacebookManager.getFriends(userID, accessToken);
            
            session.setAttribute(Constants.FB_FRIEND_LIST_KEY, rv);
        }
        
        return rv;
    }
    
    public static Set<String> getFriendIDs(HttpServletRequest request) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        Set<String> rv = new HashSet<String>();
        
        List<User> friends = getFriends(request);
        if(friends == null) {
            return null;
        }
        
        for(User friend : friends) {
            rv.add(friend.id);
        }
        
        return rv;
    }
    
    public static boolean isFriend(HttpServletRequest request, String userID) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        Set<String> friendIDs = getFriendIDs(request);
        if(friendIDs == null) {
            return false;
        }
        
        return friendIDs.contains(userID);
    }
    
    public static String getFriendName(HttpServletRequest request, String friendID) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, SQLException {
        List<User> friends = getFriends(request);
        if(friends == null) {
            return null;
        }
        
        for(User friend : friends) {
            if(friendID.equals(friend.id)) {
                return friend.name;
            }
        }
        return null;
    }
    
    private static boolean hasValidAccessToken(HttpSession session) {
        String userID = (String)session.getAttribute(Constants.FB_USERID_KEY);
        Long expirationTime = (Long)session.getAttribute(Constants.FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY);
        Util.log("hasValidAccessToken: %s, %s, %s, %b", userID, expirationTime, System.currentTimeMillis()/1000, userID != null && expirationTime != null && expirationTime > System.currentTimeMillis()/1000);
        
        return userID != null && expirationTime != null && expirationTime > System.currentTimeMillis()/1000;
    }
    
    public static void logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        
        session.removeAttribute(Constants.FB_USERID_KEY);
        session.removeAttribute(Constants.FB_ACCESS_TOKEN_KEY);
        session.removeAttribute(Constants.FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY);
    }
}
