package ws.fla.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import ws.fla.services.ExceptionReportingService;
import ws.fla.services.http.HTTPService;


public class Analytics {
    
    private static final String API_TOKEN = "dbb0c0ae648515022ca0fd25ac62ffd1";
    private static final String API_URL = "http://api.mixpanel.com/track/";
    
    private static final String SERVER_DUMMY_USER_ID = "grumblee";
    
    public static void log(String deviceID, String event, Map<String, String> props) {
        try {
            Map<String, String> params = getParams(deviceID, event, props);
            HTTPService.getAsync(API_URL, params);
            
        } catch (Exception e) {
            e.printStackTrace(System.err);
            ExceptionReportingService.submit(e);
        }
    }
    
    public static void log(String deviceID, String event, Object... params) {
        log(deviceID, event, null, params);
    }
    
    public static void log(String deviceID, String event, Map<String, String> props, Object... params) {
        Object[] paramsArray = params;
        if(paramsArray.length%2!=0) {
            throw new IllegalArgumentException("the number of vararg parameters should be even");
        }
        
        if(props == null) {
            props = new HashMap<String, String>();    
        }
        
        for(int i=0; i<params.length; i+=2) {
            String key = params[i].toString();
            String val = params[i + 1].toString();
            
            props.put(key, val);
        }
        
        log(deviceID, event, props);
    }
    
    public static void log(HttpServletRequest request, String event, Object... params) {
        String userID = RequestUtil.getUserID(request);
        Map<String, String> browserProps = getBrowserGroupProperties(request);
        log(userID, event, browserProps, params);
    }
    
    public static void serverLog(String event, Object... params) {
        log(SERVER_DUMMY_USER_ID, event, params);
    }
    
    private static Map<String, String> getParams(String deviceID, String event, Map<String, String> props) throws JSONException {
        JSONObject jsonParams = new JSONObject();
        jsonParams.put("event", event);
        
        JSONObject jsonProperties = new JSONObject();
        jsonProperties.put("token", API_TOKEN);
        jsonProperties.put("distinct_id", deviceID);
        for (Entry<String, String> p : props.entrySet()) {
            jsonProperties.put(p.getKey(), p.getValue());
        }
        
        
        jsonProperties.put("time", Math.floor((System.currentTimeMillis()/1000)));
        
        jsonParams.put("properties", jsonProperties);
        
        String data = jsonParams.toString();
        data = new String(Base64.encodeBase64(data.getBytes()));
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("data", data);
        return params;
    }
    
    public static Map<String, String> getBrowserGroupProperties(HttpServletRequest req) {
        Map<String, String > props = new HashMap<String, String>();
        
        String[] brArr = Util.browserAndVersionFromUA(req);
        String browser = brArr[0];
        String version = brArr[1];
        String os = Util.osFromUA(req);
        
        props.put("browser_version", version);      
        props.put("group_br", browser);
        props.put("group_br_v", browser+" "+version);
        props.put("group_os", os);
        props.put("group_os_br", os+" - "+browser);
        props.put("group_os_br_v", os+" - "+browser+" "+version);
        return props;
    }

}
