package ws.fla.util;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import ws.fla.api.Comment;
import ws.fla.api.Grumble;
import ws.fla.api.GrumblesManager;
import ws.fla.api.User;
import ws.fla.api.guessgame.Game;
import ws.fla.api.guessgame.Guess;
import ws.fla.api.guessgame.Hint;
import ws.fla.services.NotificationService;

public class NotificationManager {
    private static final String NOTIFICATION_HREF_BASE = String.format("http://%s/", Constants.DOMAIN);
    private static final String NOTIFICATION_TYPE_KEY =  "n_type";
    
    private static final int MAX_TEMPLATE_LENGTH = 180;
    private static final int MIN_SNIPPET_LENGTH = 12;
    
    public static void notifyOnVote(String voterID, Grumble grumble, String ofName, boolean isUpVote) throws MalformedURLException {
        notifyGrumblerOnVote(voterID, grumble, ofName, isUpVote);
        notifyGrumbleeOnVote(voterID, grumble, ofName, isUpVote);
    }
    
    private static void notifyGrumblerOnVote(String voterID, Grumble grumble, String ofName, boolean isUpVote) throws MalformedURLException {
        //don't notify self
        String recipient = grumble.byUID;
        if(voterID.equals(recipient)) {
            return;
        }    
    
        if(StringUtils.isBlank(ofName)) {
            ofName = "a friend";
        }
            
        String ref = "n_v_by";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "v_by", "g", grumble.id);
        
        //the user who the grumble is about might not have authed us so we don't tag
        String template = String.format("Someone %svoted your Grumble about %s", isUpVote ? "up" : "down", ofName);
        template = appendSnippetToTemplate(template, grumble.description);
        
        NotificationService.submit(recipient, href, template, ref);
    }
    
    private static void notifyGrumbleeOnVote(String voterID, Grumble grumble, String ofName, boolean isUpVote) throws MalformedURLException {
        //grumblee might not be authed, NotificationService will take care of NOOPing it
        
        //don't notify self
        String recipient = grumble.ofUID;
        if(voterID.equals(recipient)) {
            return;
        }    
    
        if(StringUtils.isBlank(ofName)) {
            ofName = "a friend";
        }
            
        String ref = "n_v_of";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "v_of", "g", grumble.id);
        
        String template = String.format("Someone %svoted a Grumble about you", isUpVote ? "up" : "down");
        template = appendSnippetToTemplate(template, grumble.description);
        
        NotificationService.submit(recipient, href, template, ref);
    }
    
    public static void notifyOnGrumble(String userID, Grumble grumble) throws MalformedURLException {
        if(!grumble.published) {
            return;
        }
        
        String recipient = grumble.ofUID;
        
        String ref = "n_g";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "g", "g", grumble.id);
        
        String template = String.format("Someone posted a new Grumble about you");
        template = appendSnippetToTemplate(template, grumble.description);
        
        NotificationService.submit(recipient, href, template, ref);
        
    }
    
    public static void notifyOnComment(String userID, Grumble grumble, String ofName, Comment comment) throws MalformedURLException, SQLException {
        notifyGrumblerOnComment(userID, grumble, ofName, comment);
        notifyGrumbleeOnComment(userID, grumble, comment);
        
        Set<String> commenters = GrumblesManager.getCommentersForGrumble(grumble.id);
        commenters.remove(grumble.byUID);
        commenters.remove(grumble.ofUID);
        notifyOtherCommentersOnComment(userID, commenters, grumble, ofName, comment);
        
        Set<String> voters = GrumblesManager.getVotersForGrumble(grumble.id);
        voters.remove(grumble.byUID);
        voters.remove(grumble.ofUID);
        voters.removeAll(commenters);
        notifyVotersOnComment(userID, voters, grumble, ofName, comment);
        
    }
    
    private static void notifyGrumblerOnComment(String userID, Grumble grumble, String ofName, Comment comment) throws MalformedURLException {
        String recipient = grumble.byUID;
        //don't notify self
        if(userID.equals(recipient)) {
            return;
        }
        
        if(StringUtils.isBlank(ofName)) {
            ofName = "a friend";
        }
        
        String ref = "n_c_by";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "c_by", "g", grumble.id, "c", comment.id);
        
        String template = String.format("Someone commented on a Grumble you posted about %s", ofName);
        template = appendSnippetToTemplate(template, comment.text);
        
        NotificationService.submit(recipient, href, template, ref);
    }
    
    private static void notifyGrumbleeOnComment(String userID, Grumble grumble, Comment comment) throws MalformedURLException {
        String recipient = grumble.ofUID;
        //don't notify self
        if(userID.equals(recipient)) {
            return;
        }
        
        String ref = "n_c_of";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "c_of", "g", grumble.id, "c", comment.id);
        
        String template = String.format("Someone commented on a Grumble about you");
        template = appendSnippetToTemplate(template, comment.text);
        
        NotificationService.submit(recipient, href, template, ref);
    }
    
    private static void notifyOtherCommentersOnComment(String userID, Set<String> commenters, Grumble grumble, String ofName, Comment comment) throws MalformedURLException {
        String ref = "n_c_c";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "c_c", "g", grumble.id, "c", comment.id);
        
        String template = String.format("Someone commented on a Grumble about %s that you commented on", ofName);
        template = appendSnippetToTemplate(template, comment.text);
        
        for(String commenter : commenters) {
            if(commenter.equals(userID)) {
                continue;
            }
            NotificationService.submit(commenter, href, template, ref);
        }
    }
    
    private static void notifyVotersOnComment(String userID, Set<String> voters, Grumble grumble, String ofName, Comment comment) throws MalformedURLException {
        String ref = "n_c_c";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "c_c", "g", grumble.id, "c", comment.id);
        
        String template = String.format("Someone commented on a Grumble about %s that you voted on", ofName);
        template = appendSnippetToTemplate(template, comment.text);
        
        for(String voter : voters) {
            if(voter.equals(userID)) {
                continue;
            }
            NotificationService.submit(voter, href, template, ref);
        }
    }
    
    private static String appendSnippetToTemplate(String template, String snippet) {
        int remainingSpace = MAX_TEMPLATE_LENGTH - (template.length() + 4/*1 each for quotes and colon, one for space after colon*/);
        if(remainingSpace < MIN_SNIPPET_LENGTH) {
            return template;
        }
        
        return String.format("%s: \"%s\"", template, StringUtils.abbreviate(snippet, remainingSpace));
    }
    
    public static void notifyOnNewGame(Game game, Hint firstHint) throws MalformedURLException, SQLException {
        String ref = "new_guess_game";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "new_guess_game", "gg", game.id, "h", firstHint.id);
        
        String inviterName = "Someone";
        if(!game.anonymous) {
            User hinter = GrumblesManager.getUserById(game.hinterUID);
            inviterName = hinter.name;
        }
        String template = String.format("%s invited you to play Guess The Friend", inviterName);
        template = appendSnippetToTemplate(template, firstHint.hint);
        
        NotificationService.submit(game.guesserUID, href, template, ref);
    }
    
    public static void notifyOnNewGameHint(Game game, Hint hint) throws MalformedURLException, SQLException {
        String ref = "new_guess_game_hint";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "new_guess_game_hint", "gg", game.id, "h", hint.id);
        
        String hinterName = "Someone";
        if(!game.anonymous) {
            User hinter = GrumblesManager.getUserById(game.hinterUID);
            hinterName = hinter.name;
        }
        String template = String.format("%s added a hint to a Guess The Friend game", hinterName);
        template = appendSnippetToTemplate(template, hint.hint);
        
        NotificationService.submit(game.guesserUID, href, template, ref);
    }
    
    public static void notifyOnNewGameGuess(Game game, Hint hint, Guess guess) throws MalformedURLException, SQLException {
        String ref = "new_guess_game_guess";
        String href = Util.ensureURLParameter(NOTIFICATION_HREF_BASE, NOTIFICATION_TYPE_KEY, "new_guess_game_guess", "gg", game.id, "gu", guess.id);
        
        String guesserName = "Someone";
        if(!game.anonymous) {
            User guesser = GrumblesManager.getUserById(game.guesserUID);
            guesserName = guesser.name;
        }
        String template = String.format("%s guessed in a Guess The Friend game", guesserName);
        template = appendSnippetToTemplate(template, hint.hint);
        
        NotificationService.submit(game.hinterUID, href, template, ref);
    }
}
