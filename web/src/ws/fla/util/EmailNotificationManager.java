package ws.fla.util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;

import ws.fla.api.Grumble;
import ws.fla.api.GrumblesManager;
import ws.fla.api.User;
import ws.fla.services.EmailService;
import ws.fla.services.EmailService.EmailTemplate;

public class EmailNotificationManager {
    
    public static void sendInviteToGrumblee(String grumbleID, String recipientEmail) throws SQLException {
        Grumble grumble = GrumblesManager.getGrumbleByID(grumbleID);
        User grumblee = GrumblesManager.getUserById(grumble.ofUID);
        if(recipientEmail == null) {
            recipientEmail = grumblee.email;
        }
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("invitee_first_name", grumblee.firstName);
        params.put("invitee_photo_link", String.format("https://graph.facebook.com/%s/picture?width=200", grumblee.id));
        params.put("grumble_snippet", StringUtils.abbreviate(grumble.description, 150));
        params.put("inviter_alias", "anonymous friend");
        params.put("grumble_url", String.format("http://%s?g=%s", Constants.DOMAIN, grumble.id));
        
        EmailService.submit(grumble.byUID, recipientEmail, "Someone Grumbled About You", EmailTemplate.INVITE_GRUMBLEE, params);
    }

}
