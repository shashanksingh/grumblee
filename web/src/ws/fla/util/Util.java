package ws.fla.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;


public class Util {
    
    // ex: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)
    private static final Pattern BROWSER_REGEX_IE = Pattern.compile("msie ([^;]*);");
    // ex: Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.9.2b4) Gecko/20091124 Firefox/9.0
    private static final Pattern BROWSER_REGEX_FF = Pattern.compile("firefox/(\\d\\d?\\.\\d)");
    
    private static DateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    private static final Logger logger = Logger.getLogger(Util.class);
    
    public static String printf(String template, Map<String, String> values) {
        Pattern pattern = Pattern.compile("\\{(.+?)\\}");
        Matcher matcher = pattern.matcher(template);

        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            String replacement = values.get(matcher.group(1));
            builder.append(template.substring(i, matcher.start()));
            if (replacement == null)
                builder.append(matcher.group(0));
            else
                builder.append(replacement);
            i = matcher.end();
        }
        builder.append(template.substring(i, template.length()));
        return builder.toString();
    }
    
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
    
    public static String encodeURIComponent(String component) {
        if(component == null) return null;
        String result = null;

        try {
            result = URLEncoder.encode(component, "UTF-8")
                    .replaceAll("\\%28", "(").replaceAll("\\%29", ")")
                    .replaceAll("\\+", "%20").replaceAll("\\%27", "'")
                    .replaceAll("\\%21", "!").replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            result = component;
        }

        return result;
    }
    
    public static boolean fileExists(String path) {
        File f = new File(path);
        return f.exists();
    }
    
    public static void log(String format, Object...params) {
        String m = String.format("%s: %s", sdf.format(new Date()), String.format(format, params));
        System.out.println(m);
        logger.debug(m);
    }

    public static void setCookie(HttpServletResponse response, String name, String value, String domain) {
        Cookie cookie = new Cookie (name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        cookie.setDomain(domain);
        cookie.setPath("/");
        
        response.addCookie(cookie);
    }
    
    public static String getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(name)) {
                    return c.getValue();
                }
            }
        }
        return null;
    }
    
    //cool: http://stackoverflow.com/questions/5082314/power-of-2-formula-help
    public static boolean isPowerOfTwo(int n) {
        return 2*n == (n ^ (n-1)) + 1;
    }
    
    public static String createPreparedStatementFragmentForInQuery(int numParams) {
        String[] questionMarks = new String[numParams];
        Arrays.fill(questionMarks, "?");
        return StringUtils.join(questionMarks, ",");
    }
    
    public static void tempRedirectNoCache(HttpServletResponse response, String destination) throws IOException {
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        
        response.sendRedirect(destination);
    }
    
    private static Map<String, String> getURLParameters(URL url) {
        String query = url.getQuery();
        if(query == null) {
            query = "";
        }
        
        Map<String, String> params = new HashMap<String, String>();
        String[] kvPairs = query.split("&");
        for(String kvPair : kvPairs) {
            String[] parts = kvPair.split("=");
            params.put(parts[0], parts.length == 1 ? "" : parts[1]);
        }
        return params;
    }
    
    public static boolean urlHasParameter(HttpServletRequest request, String name, String value) throws MalformedURLException {
        return urlHasParameter(Util.getFullURL(request), name, value);
    }
    
    public static boolean urlHasParameter(String urlString, String name, String value) throws MalformedURLException {
        URL url = new URL(urlString);
        String foundValue = getURLParameters(url).get(name);
        if(value == null) {
            return foundValue != null;
        }
        else {
            return value.equals(foundValue);
        }
    }
    
    public static String ensureURLParameter(String urlString, String... kv) throws MalformedURLException {
        if(kv.length%2 != 0) {
            throw new IllegalArgumentException("kv should have even values");
        }
        
        URL url = new URL(urlString);
        
        Map<String, String> params = getURLParameters(url);
        
        for(int i=0; i<kv.length; i+=2) {
            params.put(Util.encodeURIComponent(kv[i]), Util.encodeURIComponent(kv[i+1]));
        }
        
        List<String> kvPairList = new ArrayList<String>();
        for(String k : params.keySet()) {
            String v = params.get(k);
            kvPairList.add(String.format("%s=%s", k, v));
        }
        
        String query = StringUtils.join(kvPairList, '&');
        
        String ref = url.getRef();
        
        return String.format("%s://%s%s%s%s", url.getProtocol(), url.getAuthority(), url.getPath(), query.isEmpty() ? "" : "?" + query, ref == null ? "" : "#" + ref);
    }
    
    public static URL replaceDomain(URL url, String newDomain) throws MalformedURLException {
        
        String host = newDomain;
        int port = url.getPort();
        if(port != -1) {
            host = String.format("%s:%d", host, port);
        }
        
        String ref = url.getRef();
        if(ref == null) {
            ref = "";
        }
        else {
            ref = String.format("#%s", ref);
        }
        
        return new URL(String.format("%s://%s%s%s", url.getProtocol(), host, url.getFile(), ref));
    }
    
    public static String getFullURL(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String query = request.getQueryString();
        if(query != null) {
            url.append("?").append(query);
        }
        
        return url.toString();
    }
    
    public static String osFromUA(HttpServletRequest request) {
        String ag = request.getHeader("User-Agent");
        if (ag == null)
            return "UNKNOWN";
        ag = ag.toLowerCase();
        if (ag.contains("windows 3.11"))
            return "Win16";
        if (ag.contains("windows 95"))
            return "Win95";
        if (ag.contains("windows 98") || ag.contains("win98"))
            return "Win98";
        if (ag.contains("windows 2000") || ag.contains("windows nt 5.0")
                || ag.contains("windows nt 5.01"))
            return "Windows 2000";
        if (ag.contains("windows xp") || ag.contains("windows nt 5.1"))
            return "Windows XP";
        if (ag.contains("windows server 2003") || ag.contains("windows nt 5.2"))
            return "Windows NT 5.2";
        if (ag.contains("windows vista") || ag.contains("windows nt 6.0"))
            return "Windows Vista";
        if (ag.contains("windows 7") || ag.contains("windows nt 6.1"))
            return "Windows 7";
        if (ag.contains("windows 8") || ag.contains("windows nt 6.2"))
            return "Windows 8";
        if (ag.contains("windows nt 4.0") || ag.contains("windows nt"))
            return "Win NT";
        if (ag.contains("windows me"))
            return "Windows ME";
        if (ag.contains("windows ce"))
            return "Windows CE";
        if (ag.contains("ipod;")) {
            return "mob_iPod";
        }
        if (ag.contains("iphone;")) {
            return "mob_iPhone";
        }
        if (ag.contains("ipad;")) {
            return "tab_iPad";
        }
        if (ag.contains("mac os"))
            return "Mac OS";
        if (ag.contains("symbianos") || ag.contains("series 60")
                || ag.contains("series60")) {
            return "mob_symbian";
        }
        if (ag.contains("windows phone")) {
            return "mob_windows";
        }
        if (ag.contains("j2me")) {
            return "mob_J2ME";
        }
        if (ag.contains("blackberry")) {
            return "mob_Blackberry";
        }
        if (ag.contains("android")) {
            if (ag.contains("mobile")) {
                return "mob_Android";
            }
            else {
                return "tab_Android";
            }
        }
        if (ag.contains("nokia")) {
            return "mob_Nokia";
        }
        if (ag.contains("bada/") || ag.contains("samsung")) {
            return "mob_Samsung";
        }
        if (ag.contains("sonyericsson")) {
            return "mob_SonyEricsson";
        }

        if (ag.contains("libwww-perl") || ag.contains("web spider")
                || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
                || ag.contains("httpclient")) {
            return "Spider";
        }

        if (ag.contains("open bsd"))
            return "OpenBSD";
        if (ag.contains("sun os") || ag.contains("sunos"))
            return "SunOS";
        if (ag.contains("linux"))
            return "Linux";

        return "UNKNOWN";
    }
    
    public static String browserFromUA(HttpServletRequest request) {
        String ag = request.getHeader("User-Agent");
        if (ag == null)
            return "UNKNOWN";

        ag = ag.toLowerCase();
        if (ag.contains("msie")) {
            //trying to see through compatibility mode
            if (ag.contains("trident/4.0")) {
                return "IE 8.0";
            }
            else if (ag.contains("trident/5.0")) {
                return "IE 9.0";
            }
            else if (ag.contains("trident/6.0")) {
                return "IE 10.0";
            }
            
            Matcher m = BROWSER_REGEX_IE.matcher(ag);
            if (m.find()) {
                return "IE " + m.group(1);
            }
            return "IE";
        } else if (ag.contains("opera"))
            return "OP";
        else if (ag.contains("chrome"))
            return "CR";
        else if (ag.contains("firefox")) {
            Matcher m = BROWSER_REGEX_FF.matcher(ag);
            if (m.find()) {
                return "FF " + m.group(1);
            }
            return "FF";
        } else if ((ag.contains("safari") && ag.contains("version"))
                || ag.contains("mobile safari"))
            return "SF";

        if (ag.contains("iphone;")) {
            return "iPhone";
        }
        if (ag.contains("ipad;")) {
            return "iPad";
        }

        if (ag.contains("libwww-perl") || ag.contains("web spider")
                || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
                || ag.contains("httpclient")) {
            return "Spider";
        }
        if (ag.contains("blackberry")) {
            return "Blackberry";
        }
        if (ag.contains("browserng") || ag.contains("nokia")) {
            return "Nokia";
        }
        if (ag.contains("pantech")) {
            return "Pantech";
        }
        if (ag.contains("dolfin/")) {
            return "Dolfin";
        }

        return "UNKNOWN";
    }
    
    public static String[] browserAndVersionFromUA(HttpServletRequest request) {
        String browser = Util.browserFromUA(request);
        String version = "";
        String[] brArr = browser.split(" ");
        if (brArr.length > 1) {
            browser = brArr[0];
            version = brArr[1];
        }
        return new String[]{browser, version};
    }
    
    public static void main(String[] args) throws MalformedURLException, JSONException {
        //System.out.println(new JSONObject(StringEscapeUtils.escapeJavaScript(StringEscapeUtils.unescapeHtml("{&quot;message&quot;:&quot;Script error.&quot;,&quot;url&quot;:&quot;http://widgets.amung.us/classic.js&quot;,&quot;line&quot;:0}"))).toString(3));
        //System.out.println(StringEscapeUtils.unescapeHtml("{&quot;message&quot;:&quot;Script error.&quot;,&quot;url&quot;:&quot;http://widgets.amung.us/classic.js&quot;,&quot;line&quot;:0}"));
        //new JSONObject("{\"message\":\"Script error.\",\"url\":\"http:\/\/widgets.amung.us\/classic.js\",\"line\":0}");
        //System.out.println("/font/fontawesome-webfont.wofff".matches(".*\\.(ttf|eot|woff|svg)$"));
        System.out.println(StringEscapeUtils.unescapeHtml("http://grumbl.ee/?&amp;gg=1&amp;n_type=new_guess_game&amp;h=1&amp;fb_source=notification&amp;fb_ref=new_guess_game&amp;ref=notif&amp;notif_t=app_notification&amp;signed_request=5SlS4Sqqr22OFOlXhSKRwgANxQ9O-1jb1N_dJlaFr2w.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImV4cGlyZXMiOjEzNzU2MTc2MDAsImlzc3VlZF9hdCI6MTM3NTYxMDU0NSwib2F1dGhfdG9rZW4iOiJDQUFDSHdnc0oxUjRCQUlaQWVoUEFiWkFkdUFDM01mMVlYWFJhU3VFazloSFF2UXRiSVM5MmMzazRCYk9NS1VTZk55NW96RVpCWWt0Y1lxa1pCT0dOR0lxM2k5M2VRZ3JaQU9GeVBZWHlTOXltaTlxekhaQlpDYWd2SlpCNkVZV3pla3VURkloWXplWkI0bVlwUjh5QTNzNWhPcnpZUE1lcG5HVElaRCIsInVzZXIiOnsiY291bnRyeSI6InVzIiwibG9jYWxlIjoiZW5fVVMiLCJhZ2UiOnsibWluIjoyMX19LCJ1c2VyX2lkIjoiMTAwMDA0NDMxNjkyMjA4In0&amp;locale=en_US"));
    }
}
