package ws.fla.api;

public class Vote {
    public String id;
    public String voterID;
    public String grumbleID;
    public int voteType;
    public long timestamp;
    
    public Vote(String id, String voterID, String grumbleID, int voteType, long timestamp) {
        this.id = id;
        this.voterID = voterID;
        this.grumbleID = grumbleID;
        this.voteType = voteType;
        this.timestamp = timestamp;
    }
}
