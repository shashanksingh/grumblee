package ws.fla.api;

import org.json.JSONException;
import org.json.JSONObject;

public class GrumbleeGuess {
    public String id;
    public String grumbleID;
    public String guesserUID;
    public String guessUID;
    public long ts;
    
    public GrumbleeGuess(String id, String grumbleID, String guesserUID, String guessUID, long ts) {
        this.id = id;
        this.grumbleID = grumbleID;
        this.guesserUID = guesserUID;
        this.guessUID = guessUID;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        rv.put("grumble_id", this.grumbleID);
        //never leak guesserUID to anyone
        rv.put("is_by_viewer", this.guesserUID.equals(viewerUID));
        rv.put("guess_uid", this.guessUID);
        rv.put("ts", this.ts);
        
        return rv;
    }
    
}
