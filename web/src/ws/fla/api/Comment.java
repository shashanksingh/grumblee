package ws.fla.api;

import org.json.JSONException;
import org.json.JSONObject;

public class Comment {
    public String id;
    public String grumbleID;
    public String commenterUID;
    public String commenterAliasID;
    public String text;
    public long ts;
    
    
    public Comment(String id, String grumbleID, String commenterUID, String commenterAliasID, String text, long ts) {
        this.id = id;
        this.grumbleID = grumbleID;
        this.commenterUID = commenterUID;
        this.commenterAliasID = commenterAliasID;
        this.text = text;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        rv.put("grumble_id", this.id);
        //never leak commenterUID to anyone
        rv.put("is_by_viewer", this.commenterUID.equals(viewerUID));
        rv.put("commenter_alias_id", this.commenterAliasID);
        rv.put("text", this.text);
        rv.put("ts", this.ts);
        
        return rv;
    }
    
    
}
