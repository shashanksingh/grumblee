package ws.fla.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Grumble {
    public String id;
    public String byUID;
    public String ofUID;
    public boolean hiddenFromTarget;
    public boolean visibleOnlyToTarget;
    public boolean published;
    public String description;
    public long ts;
    
    public int upVoteCount;
    public int downVoteCount;
    public int commentCount;
    
    public Map<String, String> aliases = new HashMap<String, String>();
    
    public Grumble(String id, String byUID, String ofUID, boolean hiddenFromTarget, boolean visibleOnlyToTarget, boolean published, String description, long ts) {
        this.id = id;
        this.byUID = byUID;
        this.ofUID = ofUID;
        this.hiddenFromTarget = hiddenFromTarget;
        this.visibleOnlyToTarget = visibleOnlyToTarget;
        this.published = published;
        this.description = description;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        //never leak byUID to anyone
        rv.put("is_by_viewer", this.byUID.equals(viewerUID));
        rv.put("of_uid", this.ofUID);
        rv.put("hidden_from_target", this.hiddenFromTarget);
        rv.put("visible_only_to_target", this.visibleOnlyToTarget);
        rv.put("published", this.published);
        rv.put("description", this.description);
        rv.put("ts", this.ts);
        
        rv.put("upvote_count", this.upVoteCount);
        rv.put("downvote_count", this.downVoteCount);
        rv.put("comment_count", this.commentCount);
        
        rv.put("viewer_alias", aliases.containsKey(viewerUID) ? aliases.get(viewerUID) : JSONObject.NULL);
        rv.put("by_alias", aliases.containsKey(this.byUID) ? aliases.get(this.byUID) : JSONObject.NULL);
        rv.put("taken_aliases", new JSONArray(aliases.values()));
        
        return rv;
    }
}
