package ws.fla.api.guessgame;

import org.json.JSONException;
import org.json.JSONObject;

public class Game {
    public String id;
    public String hinterUID;
    public String guesserUID;
    public String guesseeUID;
    public boolean anonymous;
    public long ts;
    
    public Game(String id, String hinterUID, String guesserUID, String guesseeUID, boolean anonymous, long ts) {
        this.id = id;
        this.hinterUID = hinterUID;
        this.guesserUID = guesserUID;
        this.guesseeUID = guesseeUID;
        this.anonymous = anonymous;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        rv.put("anonymous", this.anonymous);
        if(!anonymous) {
            rv.put("hinter_uid", this.hinterUID);
            rv.put("guesser_uid", this.guesserUID);
        }
        
        rv.put("viewer_is_hinter", this.hinterUID.equals(viewerUID));
        rv.put("viewer_is_guesser", this.guesserUID.equals(viewerUID));
        
        if(this.hinterUID.equals(viewerUID)) {
            rv.put("guessee_uid", this.guesseeUID);
        }
        
        rv.put("ts", this.ts);
        
        return rv;
    }
    
}
