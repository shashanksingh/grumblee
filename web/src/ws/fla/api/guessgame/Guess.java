package ws.fla.api.guessgame;

import org.json.JSONException;
import org.json.JSONObject;

public class Guess {
    public String id;
    public String hintID;
    public String guessUID;
    public boolean isCorrect;
    public long ts;
    
    public Guess(String id, String hintID, String guessUID, boolean isCorrect, long ts) {
        this.id = id;
        this.hintID = hintID;
        this.guessUID = guessUID;
        this.isCorrect = isCorrect;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        rv.put("hint_id", this.hintID);
        rv.put("guess_uid", this.guessUID);
        rv.put("is_correct", this.isCorrect);
        rv.put("ts", this.ts);
        
        return rv;
    }
    
}
