package ws.fla.api.guessgame;

import org.json.JSONException;
import org.json.JSONObject;

public class Hint {
    public String id;
    public String gameID;
    public String hint;
    public long ts;
    
    public Hint(String id, String gameID, String hint, long ts) {
        this.id = id;
        this.gameID = gameID;
        this.hint = hint;
        this.ts = ts;
    }
    
    public JSONObject toJSON(String viewerUID) throws JSONException {
        JSONObject rv = new JSONObject();
        rv.put("id", this.id);
        rv.put("gameID", this.gameID);
        rv.put("hint", this.hint);
        rv.put("ts", this.ts);
        
        return rv;
    }
    
}
