package ws.fla.infra;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ws.fla.util.Util;

import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.UserAgent;

public class MobileRedirectFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //Util.log("doFilter: %s", ((HttpServletRequest)request).getRequestURL().toString());
        
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        
        String userAgentHeader = httpRequest.getHeader("User-Agent");
        if(userAgentHeader != null) {
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentHeader);
            
            DeviceType deviceType = userAgent.getOperatingSystem().getDeviceType();
            Util.log("deviceType: %s", deviceType.toString());
            
            if((deviceType == DeviceType.MOBILE || deviceType == DeviceType.TABLET) && !isDomainMobile(httpRequest)) {
                redirectToMobileDomain(httpRequest, httpResponse);
                return;
            }    
        }
        
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }
    
    private static void redirectToMobileDomain(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Util.log("mobile redirection: %s", request.getRemoteAddr());
        
        String fullURL = request.getRequestURL().toString();
        String query = request.getQueryString();
        if(query != null && !query.isEmpty()) {
            fullURL = String.format("%s?%s", fullURL, query);
        }
        
        Util.log("fullURL: %s", fullURL);
        
        URL fullURLObject = new URL(fullURL);
        String host = fullURLObject.getHost();
        
        String destinationURL = Util.replaceDomain(fullURLObject, convertDomainToMobileDomain(host)).toString();
        Util.log("domain replaced url: %s", destinationURL);
        response.sendRedirect(destinationURL);
    }
    
    private static boolean isDomainMobile(HttpServletRequest request) {
        return request.getServerName().startsWith("m.");
    }
    
    private static String convertDomainToMobileDomain(String host) {
        if(host.indexOf("m.") == 0) {
            return host;
        }
        host = host.replaceAll("^www[.]", "");
        return String.format("m.%s", host);
    }

}
