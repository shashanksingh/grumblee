package ws.fla.infra;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.exception.ExceptionUtils;

import ws.fla.services.EmailService;
import ws.fla.services.ExceptionReportingService;
import ws.fla.services.FacebookTokenExtensionService;
import ws.fla.services.GrumbleAutoPublishingService;
import ws.fla.services.NotificationService;
import ws.fla.services.VoteNotificationService;
import ws.fla.services.http.HTTPService;
import ws.fla.util.EmailNotificationManager;


public class ServerStartListener implements ServletContextListener {
    
    private static void reportException(Throwable t) {
        t.printStackTrace(System.out);
        //AlertService.sendErrorAlert("Error in ServerStartListener", ExceptionUtils.getFullStackTrace(t));
    }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	    
		
		try {
			HTTPService.destroy();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
        try {
            FacebookTokenExtensionService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
        try {
            ExceptionReportingService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            NotificationService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            VoteNotificationService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            EmailService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            GrumbleAutoPublishingService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
	    
	    try {
            ExceptionReportingService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
	    
	    try {
			ConnectionPool.init();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
			HTTPService.init();
		}
		catch(Throwable t) {
			reportException(t);
		}
        
		try {
            FacebookTokenExtensionService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
		try {
            NotificationService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
		try {
            VoteNotificationService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
		try {
            EmailService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
		try {
            GrumbleAutoPublishingService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
	}

}
