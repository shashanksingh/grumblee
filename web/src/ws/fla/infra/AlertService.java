package ws.fla.infra;

import ws.fla.services.EmailService;

public class AlertService {
    private static final String[] ALERT_NOTIFICATION_EMAIL_LIST = new String[]{
        "shashank.sunny.singh@gmail.com",
        "i.lakhyzha@gmail.com"
    };
    
    public static void sendErrorAlert(String title, String message) {
        sendErrorAlert(title, message, null);
    }
    
    public static void sendErrorAlert(String title, String message, String html) {
        for(String email : ALERT_NOTIFICATION_EMAIL_LIST) {
            try {
                EmailService.sendEmail(email, "Grumblee Error Alert: " + title, message, html);    
            }
            catch(Throwable t) {
                t.printStackTrace(System.err);
            }
        }
    }
}
