package ws.fla.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONException;

import ws.fla.api.GrumblesManager;
import ws.fla.services.http.HTTPService;
import ws.fla.services.http.HTTPServiceException;
import ws.fla.services.http.HTTPServiceResponse;
import ws.fla.util.Constants;
import ws.fla.util.FacebookManager;

public class FacebookTokenExtensionService {
    
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final String userID, final String accessToken) {
        
        processor.submit(new Runnable() {
            @Override
            public void run() {
                extendAccessToken(userID, accessToken);
            }
        });
    }
    
    public static void submit(final String authCode) {
        processor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String accessToken = FacebookManager.getAccessTokenForCode(authCode);
                    String userID = FacebookManager.verifyAccessToken(accessToken).getString("user_id");
                    extendAccessToken(userID, accessToken);
                } catch (JSONException | HTTPServiceException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private static void extendAccessToken(String userID, String accessToken) {
        try {
            Object[] accessTokenInfo = getExtendedAccessTokenInfo(accessToken);
            String extendedAccessToken = (String)accessTokenInfo[0];
            
            int expirationDelta = (Integer)accessTokenInfo[1];
            long expirationTime = (System.currentTimeMillis()/1000) + expirationDelta;
            
            GrumblesManager.updateAccessTokenForUser(userID, extendedAccessToken, expirationTime);
        }
        catch(HTTPServiceException e) {
            e.printStackTrace();
            ExceptionReportingService.submit(e);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            ExceptionReportingService.submit(e);
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionReportingService.submit(e);
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionReportingService.submit(e);
        }
    }
    
    private static Object[] getExtendedAccessTokenInfo(String accessToken) throws HTTPServiceException, UnsupportedEncodingException, IOException {
        String url = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=fb_exchange_token&fb_exchange_token=%s";
        url = String.format(url, Constants.FACEBOOK_APP_ID, Constants.FACEBOOK_APP_SECRET, accessToken);
        
        HTTPServiceResponse res = HTTPService.get(url);
        if(!res.isSuccess()) {
            throw new HTTPServiceException(res);
        }
        
        String data = res.getDataAsString();
        String[] kvPairs = data.split("&");
        
        String extendedAccessToken = kvPairs[0].split("=")[1];
        int expirationDelta = Integer.parseInt(kvPairs[1].split("=")[1]);
        return new Object[]{extendedAccessToken, expirationDelta};
         
    }
}

