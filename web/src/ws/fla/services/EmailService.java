package ws.fla.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import ws.fla.infra.ConnectionPool;
import ws.fla.util.Util;


public class EmailService {
    
    private static final String SMTP_HOST = "smtp.sendgrid.net";
    private static final String USERNAME = "snappe";
    private static final String PASSWORD = "mysendgridpwd";
    private static final String FROM_EMAIL_ADDRESS = "notifications@grumbl.ee";
    private static final String FROM_NAME = "Grumblee";
    
    private static final String UNSUB_LINK_TEMPLATE = "http://grumbl.ee/unsubscribe.jsp?id=%s";
    
    private static Session mailSession;
    private static ExecutorService processor;
    
    static {
        
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", "true");
        
        Authenticator auth = new SMTPAuthenticator();
        mailSession = Session.getInstance(props, auth);
        
    }
    
    public static enum EmailTemplate {
        INVITE_GRUMBLEE    
    };
    

    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final String fromUID, final String recipientEmail, final String subject, final EmailTemplate template, final Map<String, String> templateParams) {
        processor.submit(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                try {
                    sendEmail(fromUID, recipientEmail, subject, template, templateParams);    
                }
                catch(Throwable t) {
                    ExceptionReportingService.submit(t);
                    throw t;
                }
                
                return null;
            }
        });
    }
    
    private static String getPrettyName(String name, String defaultName) {
        return StringUtils.defaultIfEmpty(name, defaultName);
    }
    
    private static String getTemplate(EmailTemplate template, boolean html) throws IOException {
        String suffix = html ? "html" : "txt";
        
        String path = String.format("emailtemplates/%s.%s", template.toString().toLowerCase(), suffix);
        InputStream is = EmailService.class.getResourceAsStream(path);
        return IOUtils.toString(is, "UTF-8");
    }
    
    private static void sendEmail(String fromUID, String recipientEmail, String subject, EmailTemplate template, Map<String, String> templateParams) throws IOException, MessagingException, SQLException {
        if(isUnsubscribedEmail(recipientEmail)) {
            Util.log("ignoring email %s to %s because the recipient is unsubscribed", subject, recipientEmail);
            return;
        }
        
        String emailID = insertEmailRecord(fromUID, recipientEmail, template, templateParams);
        
        if(templateParams.get("unsub_link") == null) {
            String unsubLink = String.format(UNSUB_LINK_TEMPLATE, Util.encodeURIComponent(emailID));
            templateParams.put("unsub_link", unsubLink);    
        }
        
        String textTemplateString = getTemplate(template, false);
        String filledTextTemplate = Util.printf(textTemplateString, templateParams);
        
        String htmlTemplateString = getTemplate(template, true);
        String filledHTMLTemplate = Util.printf(htmlTemplateString, templateParams);
        
        sendEmail(recipientEmail, subject, filledTextTemplate, filledHTMLTemplate);
        
        //Analytics.serverLog("email_sent", "to", recipientEmail, "subject", subject, "template", template);
    }
    
    public static void sendEmail(String recipientEmail, String subject, String text, String html) throws MessagingException, UnsupportedEncodingException, SQLException {
        if(text == null && html == null) {
            throw new IllegalArgumentException("at least one of text and html should be non-null");
        }
        
        if(isUnsubscribedEmail(recipientEmail)) {
            Util.log("ignoring email %s to %s because the recipient is unsubscribed", subject, recipientEmail);
            return;
        }
        
        // uncomment for debugging infos to stdout
        //mailSession.setDebug(true);
        
        Transport transport = mailSession.getTransport();
        MimeMessage message = new MimeMessage(mailSession);
        Multipart multipart = new MimeMultipart("alternative");

        if(text != null) {
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(text);
            multipart.addBodyPart(textPart);
        }

        if(html != null) {
            BodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(html, "text/html; charset=utf-8");
            multipart.addBodyPart(htmlPart);
        }        

        message.setContent(multipart);
        message.setFrom(new InternetAddress(FROM_EMAIL_ADDRESS, FROM_NAME));
        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO,
             new InternetAddress(recipientEmail));

        transport.connect();
        transport.sendMessage(message,
            message.getRecipients(Message.RecipientType.TO));
        transport.close();

    }
    
    private static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(USERNAME, PASSWORD);
        }
    }
    
    private static String insertEmailRecord(String fromUID, String toEmail, EmailTemplate type, Map<String, String> params) throws SQLException {
        String emailID = Util.getUUID();
        String paramsJSON = new JSONObject(params).toString();
        
        String q = "INSERT INTO emails (email_id, from_uid, to_email, email_type, params) VALUES (?, ?, ?, ?, ?)";
        
        Connection con = ws.fla.infra.ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            ps.setString(2, fromUID);
            ps.setString(3, toEmail);
            ps.setString(4, type.toString());
            ps.setString(5, paramsJSON);
            
            ps.executeUpdate();
            return emailID;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static boolean isUnsubscribedEmail(String email) throws SQLException {
        String q = "SELECT COUNT(1) AS matches FROM unsubscribed_emails JOIN emails ON (unsubscribed_emails.email_id = emails.email_id) WHERE to_email = ?";
        
        Connection con = ws.fla.infra.ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, email);
            
            ResultSet rs = ps.executeQuery();
            rs.next();
            
            int matchCount = rs.getInt("matches");
            return matchCount > 0;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static String getRecipientEmailForEmailID(String emailID) throws SQLException {
        String q = "SELECT to_email FROM emails WHERE email_id = ?";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                return null;
            }
            return rs.getString("to_email");
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String unsubscribe(String emailID) throws SQLException {
        String recipientEmail = getRecipientEmailForEmailID(emailID);
        if(recipientEmail == null) {
            return null;
        }
        
        String q = "INSERT INTO unsubscribed_emails (email_id) VALUES (?)";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            
            ps.executeUpdate();
            
            return recipientEmail;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        sendEmail(Util.getUUID(), "shashank@infoaxe.com", "Test", EmailTemplate.INVITE_GRUMBLEE, new HashMap<String, String>());
    }

}
