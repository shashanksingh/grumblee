package ws.fla.services;

import java.util.*;

import ws.fla.services.http.HTTPService;
import ws.fla.services.http.HTTPServiceResponse;
import ws.fla.util.Util;

public class ProfilePicCachingService {
    private static final int MAX_BUFFER_SIZE = 100;
    
    private static List<String> buffer = new ArrayList<String>();
    private static Map<String, HTTPServiceResponse> userIDToResponse = new HashMap<String, HTTPServiceResponse>();
    
    
    public static HTTPServiceResponse get(String userID) {
        Util.log("HTTPServiceResponse: buffer: %d, map: %d", buffer.size(), userIDToResponse.size());
        
        HTTPServiceResponse rv = userIDToResponse.get(userID);
        if(rv != null) {
            return rv;
        }
        
        return fetchPic(userID);
    }
    
    
    private static HTTPServiceResponse fetchPic(String userID) {
        String url = String.format("https://graph.facebook.com/%s/picture?width=90&height=90", userID);
        HTTPServiceResponse res = HTTPService.get(url);
        
        buffer.add(userID);
        userIDToResponse.put(userID, res);
        
        trimCache();
        
        return res;
    }
    
    private static void trimCache() {
        while(buffer.size() > MAX_BUFFER_SIZE) {
            String removedUserID = buffer.remove(0);
            //there could be two entries in the buffer for one in the map
            if(buffer.indexOf(removedUserID) < 0) {
                userIDToResponse.remove(removedUserID);
            }
        }
    }
}
