package ws.fla.services;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;

import ws.fla.api.Grumble;
import ws.fla.api.GrumblesManager;
import ws.fla.infra.AlertService;
import ws.fla.util.NotificationManager;
import ws.fla.util.Util;

public class GrumbleAutoPublishingService {
    
private static ScheduledExecutorService processor;
    
    private static LinkedBlockingQueue<Grumble> queue = new LinkedBlockingQueue<Grumble>();
    
    private GrumbleAutoPublishingService() {
        
    }
    
    public static synchronized void start() {
        initProcessor();
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    private static void initProcessor() {
        if(processor == null) {
            processor = Executors.newSingleThreadScheduledExecutor();
            processor.scheduleWithFixedDelay(new Runnable() {
                
                @Override
                public void run() {
                    try {
                        List<Grumble> q = new ArrayList<Grumble>();
                        queue.drainTo(q);
                        Util.log("got %d grumbles to publish", q.size());
                        publishGrumbles(q);    
                    }
                    catch(Throwable t) {
                        String message = String.format("<pre>%s</pre>", ExceptionUtils.getFullStackTrace(t));
                        AlertService.sendErrorAlert("Error in GrumbleAutoPublishingService", message);
                    }
                }
                
            }, 5, 5, TimeUnit.MINUTES);
            
        }
    }
    
    public static void submit(Grumble grumble) {
        Util.log("new grumbled queued to be published: grumbleID: %s", grumble.id);
        queue.add(grumble);
    }
    
    private static void publishGrumbles(List<Grumble> grumbles) throws SQLException, MalformedURLException {
        Set<String> grumbleIDSet = new HashSet<String>();
        for(Grumble grumble : grumbles) {
            grumbleIDSet.add(grumble.id);
        }
        
        GrumblesManager.publishGrumbles(grumbleIDSet);
        
        for(Grumble grumble : grumbles) {
            NotificationManager.notifyOnGrumble(grumble.byUID, grumble);
        }
    }

}
