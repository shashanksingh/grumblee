package ws.fla.services;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ws.fla.util.FacebookManager;
import ws.fla.util.Util;

public class NotificationService {
    
    private static ExecutorService processor;
    
    private NotificationService() {
        
    }
    
    public static synchronized void start() {
        initProcessor();
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    private static void initProcessor() {
        if(processor == null) {
            processor = Executors.newFixedThreadPool(1);
        }
    }
    
    public static void submit(final String recipientUID, final String href, final String template, final String ref) {
        processor.submit(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                try {
                    FacebookManager.sendNotification(recipientUID, href, template, ref);
                }
                catch(Throwable t) {
                    ExceptionReportingService.submit(t);
                }
                
                return null;
            }
        });
    }
}