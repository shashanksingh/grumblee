<table>
  <tr>
    <td>Hi {invitee_first_name}!</td>
  </tr>
  <tr>
    <td>A friend anonymously posted a grumble about you on Grumblee</td>
    <td>"{grumble_snippet}"</td>
  </tr>
  <tr>
    <td>view the grumble <a href="{grumble_url}">here</a></td>
  </tr>
</table>