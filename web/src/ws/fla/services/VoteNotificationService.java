package ws.fla.services;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;

import ws.fla.api.Grumble;
import ws.fla.api.GrumblesManager;
import ws.fla.api.Vote;
import ws.fla.infra.AlertService;
import ws.fla.util.NotificationManager;
import ws.fla.util.Util;

public class VoteNotificationService {
    private static ScheduledExecutorService processor;
    
    private static LinkedBlockingQueue<VoteInfo> queue = new LinkedBlockingQueue<VoteInfo>();
    
    private VoteNotificationService() {
        
    }
    
    public static synchronized void start() {
        initProcessor();
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    private static void initProcessor() {
        if(processor == null) {
            processor = Executors.newSingleThreadScheduledExecutor();
            processor.scheduleWithFixedDelay(new Runnable() {
                
                @Override
                public void run() {
                    try {
                        List<VoteInfo> q = new ArrayList<VoteInfo>();
                        queue.drainTo(q);
                        Util.log("got %d votes to notify", q.size());
                        processVotes(q);    
                    }
                    catch(Throwable t) {
                        String message = String.format("<pre>%s</pre>", ExceptionUtils.getFullStackTrace(t));
                        AlertService.sendErrorAlert("Error in ExceptionReportingService", message);
                    }
                    
                }
                
            }, 1, 1, TimeUnit.MINUTES);
            
        }
    }
    
    public static void submit(String voteID, Grumble grumble, String ofName) {
        Util.log("new vote queued: voteID: %s", voteID);
        queue.add(new VoteInfo(voteID, grumble, ofName));
    }
    
    private static void processVotes(List<VoteInfo> voteInfoList) throws SQLException, MalformedURLException {
        if(voteInfoList.isEmpty()) {
            return;    
        }
        
        Map<String, VoteInfo> voteIDToVoteInfo = new HashMap<String, VoteInfo>();
        for(VoteInfo voteInfo : voteInfoList) {
            voteIDToVoteInfo.put(voteInfo.voteID, voteInfo);
        }
        
        Set<Vote> votes = GrumblesManager.getVotesByIDs(voteIDToVoteInfo.keySet());
        for(Vote vote : votes) {
            VoteInfo voteInfo = voteIDToVoteInfo.get(vote.id);
            NotificationManager.notifyOnVote(vote.voterID, voteInfo.grumble, voteInfo.ofName, vote.voteType > 0);
        }
    }
}

class VoteInfo {
    String voteID;
    Grumble grumble;
    String ofName;
    
    public VoteInfo(String voteID, Grumble grumble, String ofName) {
        this.voteID = voteID;
        this.grumble = grumble;
        this.ofName = ofName;
    }
    
    
}
